<?php return array (
  'numPageLinks' => 10,
  'itemsPerPage' => 100,
  'copyrightNotice' => 
  array (
    'en_US' => 'Authors who submit to this conference agree to the following terms:<br /> <strong>a)</strong> Authors retain copyright over their work, while allowing the conference to place this unpublished work under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution License</a>, which allows others to freely access, use, and share the work, with an acknowledgement of the work\'s authorship and its initial presentation at this conference.<br /> <strong>b)</strong> Authors are able to waive the terms of the CC license and enter into separate, additional contractual arrangements for the non-exclusive distribution and subsequent publication of this work (e.g., publish a revised version in a journal, post it to an institutional repository or publish it in a book), with an acknowledgement of its initial presentation at this conference.<br /> <strong>c)</strong> In addition, authors are encouraged to post and share their work online (e.g., in institutional repositories or on their website) at any point before and after the conference.',
  ),
  'privacyStatement' => 
  array (
    'en_US' => 'The names and email addresses entered in this conference site will be used exclusively for the stated purposes of this conference and will not be made available for any other purpose or to any other party.',
  ),
  'archiveAccessPolicy' => 
  array (
    'en_US' => 'The presentations that make up the current and archived conferences on this site have been made open access and are freely available for viewing, for the benefit of authors and interested readers.',
  ),
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'Authors are permitted to post their papers on personal or institutional websites prior to and after publication by this conference (while providing the bibliographic details of that publication).',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This conference utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the conference for purposes of preservation and restoration. <a href="http://lockss.stanford.edu/">More...</a>',
  ),
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'paymentMethodPluginName' => 'ManualPayment',
  'rtAbstract' => true,
  'rtCaptureCite' => true,
  'rtViewMetadata' => true,
  'rtSupplementaryFiles' => true,
  'rtPrinterFriendly' => true,
  'rtAuthorBio' => true,
  'rtDefineTerms' => true,
  'rtAddComment' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'title' => 
  array (
    'en_US' => 'CAA Conference',
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'header-int.png',
      'uploadName' => 'pageHeaderTitleImage_en_US.png',
      'width' => 1003,
      'height' => 147,
      'dateUploaded' => '2014-07-28 22:46:44',
    ),
  ),
  'navItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'name' => '',
        'url' => '',
      ),
    ),
  ),
  'enableAnnouncements' => false,
  'enableAnnouncementsHomepage' => true,
  'conferenceTheme' => '',
  'homeHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'numAnnouncementsHomepage' => 2,
  'schedConfRedirect' => 0,
  'paperEventLog' => true,
  'paperEmailLog' => true,
  'conferenceEventLog' => true,
  'searchDescription' => 
  array (
    'en_US' => 'CAA Conference Registration',
  ),
  'searchKeywords' => 
  array (
    'en_US' => 'caa, caaconference, xaa international, caa 2015, caa2016, caa 2017, caa2017, atlata',
  ),
  'contactName' => 'Support',
  'contactTitle' => '',
  'contactEmail' => '2017@caaconference.org',
  'contactPhone' => '',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'restrictPaperAccess' => false,
  'enableComments' => false,
  'commentsRequireRegistration' => false,
  'commentsAllowAnonymous' => false,
  'paperAccess' => 0,
  'copyrightNoticeAgree' => false,
  'postCreativeCommons' => false,
  'customAboutItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'title' => '',
        'content' => '',
      ),
    ),
  ),
  'homeHeaderTitleImageAltText' => 
  array (
    'en_US' => 'CAA - Computer Applications and Quantitative Methods in Archaeology',
  ),
  'pageHeaderTitleImageAltText' => 
  array (
    'en_US' => 'CAA - Computer Applications and Quantitative Methods in Archaeology',
  ),
  'homeHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'pageHeaderTitleImage_en_US.png',
      'uploadName' => 'homeHeaderTitleImage_en_US.png',
      'width' => 1003,
      'height' => 148,
      'dateUploaded' => '2014-12-08 20:15:55',
    ),
  ),
  'conferencePageFooter' => 
  array (
    'en_US' => '<p style="text-align:center"><a href="http://caa-international.org">CAA International</a> | <a href="http://caaconference.org">CAA Conference</a> | <a href="https://www.members.caa-international.org">Members</a></p>',
  ),
); ?>