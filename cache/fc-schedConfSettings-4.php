<?php return array (
  'acceptSupplementaryReviewMaterials' => false,
  'closeCommentsDate' => 1406588399,
  'contactEmail' => 'campana@lapetlab.it',
  'contactFax' => '',
  'contactMailingAddress' => 'UNIVERSITY of CAMBRIDGE
Faculty of Classics
Sidgwick Avenue
Cambridge CB3 9DA UK',
  'contactName' => 'Stefano Campana',
  'contactPhone' => '+393280423331',
  'contactTitle' => '',
  'contributors' => 
  array (
  ),
  'copySubmissionAckAddress' => 'campana@lapetlab.it',
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckSpecified' => true,
  'delayOpenAccessDate' => 1406502000,
  'enableAuthorSelfArchive' => false,
  'enableOpenAccessNotification' => false,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'enableRegistrationExpiryReminderAfterMonths' => false,
  'enableRegistrationExpiryReminderAfterWeeks' => false,
  'enableRegistrationExpiryReminderBeforeMonths' => true,
  'enableRegistrationExpiryReminderBeforeWeeks' => false,
  'endDate' => 1446422400,
  'envelopeSender' => '',
  'locationAddress' => '',
  'locationCity' => 'Siena',
  'locationCountry' => 'IT',
  'locationName' => 'University of Siena',
  'metaCitations' => true,
  'metaCoverage' => false,
  'metaDiscipline' => false,
  'metaSubject' => true,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaType' => false,
  'notifyAllAuthorsOnDecision' => true,
  'numDaysBeforeInviteReminder' => 5,
  'numDaysBeforeSubmitReminder' => 1,
  'numMonthsAfterRegistrationExpiryReminder' => 0,
  'numMonthsBeforeRegistrationExpiryReminder' => 1,
  'numWeeksAfterRegistrationExpiryReminder' => 0,
  'numWeeksBeforeRegistrationExpiryReminder' => 0,
  'numWeeksPerReview' => 4,
  'numWeeksPerReviewAbsolute' => 1419033600,
  'numWeeksPerReviewRelative' => 0,
  'paymentMethodPluginName' => 'Stripe',
  'postAbstractsDate' => 1406502000,
  'postAccommodation' => false,
  'postCFP' => false,
  'postOverview' => false,
  'postPapersDate' => 1406502000,
  'postPayment' => false,
  'postPresentations' => false,
  'postProgram' => false,
  'postProposalSubmission' => false,
  'postScheduleDate' => 1406502000,
  'postSupporters' => false,
  'postTimeline' => false,
  'postTrackPolicies' => false,
  'previewAbstracts' => true,
  'rateReviewerOnQuality' => 1,
  'regAuthorCloseDate' => 1425167999,
  'regAuthorOpenDate' => 1406847600,
  'registrationEmail' => 'srlc3@cam.ac.uk',
  'registrationFax' => '',
  'registrationMailingAddress' => 'University of Siena
Ancient Topography, Laboratory of Landscape Archaeology and Remote Sensing
Department of History and Cultural Heritage
Via Roma 56, 53100 Siena
Italy',
  'registrationName' => 'Stefano Campana',
  'registrationPhone' => '',
  'regReviewerCloseDate' => 1375052399,
  'regReviewerOpenDate' => 1374966000,
  'remindForInvite' => 1,
  'remindForSubmit' => 1,
  'restrictReviewerFileAccess' => 1,
  'reviewDeadlineType' => 2,
  'reviewerAccessKeysEnabled' => 0,
  'reviewMode' => 3,
  'showCFPDate' => 1412895600,
  'sponsors' => 
  array (
  ),
  'startDate' => 1446163200,
  'submissionsCloseDate' => 1438469999,
  'submissionsOpenDate' => 1412895600,
  'supportEmail' => 'campana@lapetlab.it',
  'supportName' => 'Stefano Campana',
  'supportPhone' => '+393280423331',
  'acronym' => 
  array (
    'en_US' => 'CAA2015',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p><strong>ABSTRACT LENGTH</strong></p> <p>Your abstract should not be longer than 500 words including title, affiliations and key words.</p><p><strong>AUTHORS AND AFFILIATION</strong></p> <p>Provide the full names and affiliations of all authors, including e-mail addresses. Please indicate the name of the corresponding author.</p><p><strong>KEYWORDS</strong></p> <p>Provide 3-5 keywords describing the contents of your paper.</p> <p><strong>LANGUAGE</strong></p><p><strong> </strong>The official language of the conference is English. Spelling should conform to British      practice and follow the Oxford English Dictionary.</p> <p><strong>START THE SUBMISSION PROCESS</strong></p>',
  ),
  'cfpMessage' => 
  array (
    'en_US' => '<p><img style="width: 100%;" src="/ocs/public/site/images/admin/caa-2015-header.jpg" alt="CAA 2015 SIENA – ITALY &lt;&lt;&lt;keep the revolution going&gt;&gt;&gt;" /></p><h2>Call for papers</h2><p><strong>DEADLINE – 20 November 2014</strong></p> <p>The Organising Committee wish to inform you that the Call for Papers for the 43rd International Conference on Computer Applications and Quantitative Methods in Archaeology (CAA) is open. The conference will be held at the University of Siena (Italy), in collaboration with the National Research Council (ISTI-Pisa), from March 30th to April 3rd 2015.</p><p class="p1"><span class="s1"><strong>CALL FOR PAPERS DEADLINE 20 November 2014 </strong></span><span class="s2"><strong>at 23 h 59 min 59 sec Siena time (CET = UTC+1)</strong></span></p><p><strong> </strong>The 43rd Computer Applications and Quantitative Methods in Archaeology Conference, “KEEP THE REVOLUTION GOING” (CAA 2015 SIENA) will explore a multitude of topics to showcase ground-breaking technologies and best practice from various archaeological and computer science disciplines, with a large diversity of case studies from all over the world.</p> <p>The main themes of the conference include according to the session proposals (<strong>48 accepted sessions</strong>), the following:</p> <p><strong>1. </strong><strong>Field and laboratory data recording</strong></p> <p><strong>2. </strong><strong>Sematic, Data modelling, management and integration</strong></p> <p><strong>3. </strong><strong>Data analysis and visualisation</strong></p> <p><strong>4. </strong><strong>3D modelling, visualisation and simulations</strong></p> <p><strong>5. </strong><strong>Spatio-temporal modelling and GIS</strong></p> <p><strong>6. </strong><strong>Remote sensing</strong></p> <p><strong>7. </strong><strong>Users and interfaces: education, museums and multimedia</strong></p> <p><strong>8. </strong><strong>Cultural heritage interpretation and modelling past urban and rural contexts</strong></p> <p><strong>9. </strong><strong>Theoretical issues, and the relation of CAA with the Digital Humanities</strong></p><p><strong></strong><strong>10. Open software, open data</strong></p><p><strong></strong><strong>11. Free session</strong></p><p>The full and detailed list of the accepted sessions is available <a href="http://caaconference.org/program/">here</a>.</p>',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'University of Cambridge
University of Siena',
  ),
  'emailSignature' => 
  array (
    'en_US' => '________________________________________________________________________
CAA Conference 2015
http://caaconference.org/',
  ),
  'introduction' => 
  array (
    'en_US' => '<img style="width: 100%;" src="/ocs/public/site/images/admin/caa-2015-header.jpg" alt="" />

The 43rd Computer Applications and Quantitative Methods in Archaeology “KEEP THE REVOLUTION GOING” Conference (CAA 2015 SIENA) will explore a multitude of topics to showcase ground-breaking technologies and best practice from various archaeological and computer sciences disciplines, with a large diversity of case studies from all over the world. Some of these topics are specific to the Italian scientific community, which played since the early stage of computer applicationa central role, participating to the debate and development in particular of GIS, databases, semantic, remote sensing, 3D data collection, modeling, visualization, etc.

The conference will be held in Italy at the University of Siena in collaboration with the National Research Council, from March 30th to April 3rd 2015.

The conference usually brings together hundreds of participants coming from all over the world involving delegates in parallel sessions, workshops, tutorials and roundtables.  

Conference website: <a href="http://2015.caaconference.org">caaconference.org</a>

For general information please email: <a href="mailto:campana@lapetlab.it">info@caa2015.org</a>',
  ),
  'metaCoverageChronExamples' => 
  array (
    'en_US' => '(E.g., Early Middle-ages; Iron age). Separate terms with a semicolon. Optional.',
  ),
  'metaCoverageGeoExamples' => 
  array (
    'en_US' => '(E.g., Iberian Peninsula; Egypt). Separate terms with a semicolon. Optional.',
  ),
  'metaCoverageResearchSampleExamples' => 
  array (
    'en_US' => '(E.g., Age; Gender; Ethnicity; etc.). Separate terms with a semicolon. Optional.',
  ),
  'metaSubjectExamples' => 
  array (
    'en_US' => 'E.g., Semantic Web; surface modelling; predictive modelling). Separate terms with a semicolon.',
  ),
  'metaTypeExamples' => 
  array (
    'en_US' => '(E.g., Field recording, network visualisation, database management, ontological analysis). Separate terms with a semicolon. Optional.',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<p>The abstracts will be reviewed by members of the CAA 2014 Review College.</p><p>The role of the reviewer is twofold. Firstly, it consists of assisting the CAA 2015 Scientific Committee in assessing the quality of proposals submitted. Reviews do not altogether determine which proposals will be accepted or rejected, but rather provide expert information which Scientific Committee use in making its decisions. Each paper must be reviewed by at least three people.</p><p>The second major role for reviewers is to provide helpful, constructive feedback to authors, which can strengthen both the quality of the conference Proceedings and the collegiality and intellectual rigor of the CAA authors.</p><p>The following selection criteria will be used by the reviewers:</p><ul><li>the session’s academic standard (the extent to which the session is original, technically excellent, critical);</li><li>its consistency of content;</li><li>its clarity of style;</li></ul><p>A <strong>good review</strong> should suggest concrete ways in which the proposal may be strengthened. This feedback is important whether the reviewer is recommending that the session be rejected or accepted. If the former, it will enable the author to revise his/her session submission and strengthen it or to submit a stronger proposal next year; if the latter, it will result in a stronger session being submitted. In either case, constructive criticism projects collegiality and an interest in others’ work. It is <strong>essential that the reviewers be uncompromisingly professional and courteous </strong>in reviewing all submissions, even when their paper assessment is not positive. Comments which are purely negative should be addressed solely to the Editorial Panel.</p><p>The review procedure will include a <strong>check for English language</strong>. To this end, at least one of the reviewers will be a native English speaker. If the quality of English prevents proper assessment then the abstract can be rejected (this will have to be indicated as the reason for rejection in the Comments to the Authors box). If however reviewers feel that the content is expressed well enough for them to evaluate the submission then they can accept the submission but add a note in the Comments to the Authors box indicating that acceptance is subject to revision of the abstract’s English.</p><p>Reviewed abstract can receive one of following possible recommendations:</p><ul><li>accept as is: the abstract is of the appropriate quality and fits with the aims of the conference as a whole.</li><li>accept with revisions (minor or major revision) (including English language, referencing and formatting): the abstract is of sufficient level but requires (minor or major) revision. The reviewers will have to provide the authors with comments that will help them to improve their manuscript and, if needed, a brief comment on referencing and formatting. Authors of abstracts requiring major revisions will have to resubmit their papers accompanied by a brief statement of how the reviewers’ comments have been addressed.</li><li>reject: the abstract is not of the appropriate quality or it does not fit to the conference as a whole. Rejected manuscript cannot be resubmitted.</li></ul><p> </p><p> </p><p> </p><p> </p><p>[We gratefully acknowledge use of extracts from the Digital Humanities conference guide for reviewers]</p>',
  ),
  'reviewPolicy' => 
  array (
    'en_US' => '<p>The session proposal abstract will be reviewed by members of the CAA 2014 Review College. Each abstract must be reviewed by at least three people.</p><p>The following selection criteria will be used by the reviewers:</p><ul><li>the session’s academic standard (the extent to which the session is original, technically excellent, critical);</li><li>its consistency of content;</li><li>its clarity of style;</li></ul><p>The review procedure will include a check for English language. To this end, at least one of the reviewers will be a native English speaker. If the quality of English prevents proper assessment then the session can be rejected (this will have to be indicated as the reason for rejection in the Comments to the Authors box).</p><p>Reviewed sessions can receive one of following possible recommendations:</p><ul><li>accept as is: the session is of the appropriate quality and fits with the aims of the conference as a whole.</li><li>accept with revisions (minor or major revision) (including English language, referencing and formatting): the session is of sufficient level but requires (minor or major) revision. The reviewers will have to provide the authors with comments that will help them to improve their session proposal. Authors of sessions requiring major revisions will have to resubmit their abstracts accompanied by a brief statement of how the reviewers’ comments have been addressed.</li><li>reject: the session is not of the appropriate quality or it does not fit to the conference as a whole. Rejected abstract cannot be resubmitted.</li></ul>',
  ),
  'title' => 
  array (
    'en_US' => 'CAA 2015',
  ),
); ?>