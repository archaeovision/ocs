<?php return array (
  'acceptSupplementaryReviewMaterials' => false,
  'closeCommentsDate' => 1500332399,
  'contactEmail' => 'caa2018@caaconference.org',
  'contactFax' => '',
  'contactMailingAddress' => 'eScience-Center / UB
Wilhelmstr. 32
72074 Tübingen
Germaby',
  'contactName' => 'Matthias Lang',
  'contactPhone' => '+49 7071 2972837',
  'contactTitle' => '',
  'contributors' => 
  array (
  ),
  'copySubmissionAckAddress' => '',
  'copySubmissionAckPrimaryContact' => true,
  'copySubmissionAckSpecified' => false,
  'delayOpenAccessDate' => 1500246000,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'endDate' => 1521763200,
  'envelopeSender' => '',
  'locationAddress' => '',
  'locationCity' => 'Tübingen',
  'locationCountry' => 'DE',
  'locationName' => 'University of Tübingen',
  'metaCitations' => false,
  'metaCoverage' => false,
  'metaDiscipline' => false,
  'metaSubject' => true,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaType' => false,
  'notifyAllAuthorsOnDecision' => true,
  'numDaysBeforeInviteReminder' => 3,
  'numDaysBeforeSubmitReminder' => 2,
  'numWeeksPerReview' => 4,
  'numWeeksPerReviewAbsolute' => 1537138800,
  'numWeeksPerReviewRelative' => 0,
  'paymentMethodPluginName' => 'Stripemanual',
  'postAbstractsDate' => 1500246000,
  'postAccommodation' => false,
  'postCFP' => false,
  'postOverview' => false,
  'postPapersDate' => 1500246000,
  'postPayment' => false,
  'postPresentations' => false,
  'postProgram' => false,
  'postProposalSubmission' => false,
  'postScheduleDate' => 1500246000,
  'postSupporters' => false,
  'postTimeline' => false,
  'postTrackPolicies' => false,
  'previewAbstracts' => false,
  'rateReviewerOnQuality' => 0,
  'regAuthorCloseDate' => 1531868399,
  'regAuthorOpenDate' => 1500159600,
  'regReviewerCloseDate' => 1501541999,
  'regReviewerOpenDate' => 1500246000,
  'remindForInvite' => 1,
  'remindForSubmit' => 1,
  'restrictReviewerFileAccess' => 1,
  'reviewDeadlineType' => 2,
  'reviewerAccessKeysEnabled' => 0,
  'reviewMode' => 0,
  'showCFPDate' => 1500159600,
  'sponsors' => 
  array (
  ),
  'startDate' => 1521417600,
  'submissionsCloseDate' => 1517270399,
  'submissionsOpenDate' => 1500159600,
  'supportEmail' => 'michael.derntl@uni-tuebingen.de',
  'supportName' => 'Michael Derntl',
  'supportPhone' => '',
  'acronym' => 
  array (
    'en_US' => 'CAA2018',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p><strong>ABSTRACT LENGTH</strong></p> <p>Your paper or poster abstract should not be longer than 250 words excluding title, affiliations and key words.</p><p><strong>AUTHORS AND AFFILIATION</strong></p> <p>Provide the full names and affiliations of all authors, including e-mail addresses. Please indicate the name of the corresponding author.</p><p><strong>KEYWORDS</strong></p> <p>Provide 3-5 keywords describing your session.</p> <p><strong>LANGUAGE</strong></p><p><strong> </strong>The official language of the conference is English. Spelling should conform to British practice and follow the Oxford English Dictionary.</p> <p><strong>START THE SUBMISSION PROCESS</strong></p>',
  ),
  'cfpMessage' => 
  array (
    'en_US' => 'Please submit your paper or poster proposal',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'eScience-Center 
University of Tübingen',
  ),
  'emailSignature' => 
  array (
    'en_US' => '________________________________________________________________________
CAA Conference CAA 2018
http://ocs.caaconference.org/index.php?conference=caa&schedConf=CAA2018&page=index',
  ),
  'introduction' => 
  array (
    'en_US' => '<img style="width: 100%;" src="http://2018.caaconference.org/wp-content/uploads/sites/22/2017/07/cropped-header.jpg" width="100%" alt="" />

The 46th Computer Applications and Quantitative Methods in Archaeology Conference (CAA 2018) has been given the theme “Human history and digital future”. The conference will address a multitude of topics. Through diverse case studies from all over the world, the conference will show new technical approaches and best practice from various archaeological and computer-science disciplines. The conference will bring together hundreds of participants from around the world in parallel sessions, workshops, tutorials and roundtables.

We warmly welcome participants and contributors to the beautiful medieval town of Tübingen.',
  ),
  'metaSubjectExamples' => 
  array (
    'en_US' => 'E.g., GIS; Geophysics; 3D Visualization; Data Management',
  ),
  'title' => 
  array (
    'en_US' => 'CAA 2018',
  ),
); ?>