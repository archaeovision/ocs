<?php return array (
  'plugins.paymethod.stripemanual.displayName' => 'Stripe & manual',
  'plugins.paymethod.stripemanual.settings.stripeenable' => 'Enable Stripe& manual',
  'plugins.paymethod.stripemanual.settings.stripemanualurl' => 'Stripe URL',
  'plugins.paymethod.stripemanual.settings.stripemanualurl.description' => 'This is the URL to Stripe\'s  service. https://checkout.stripe.com/v2/checkout.js',
  'plugins.paymethod.stripemanual.settings.secret_key' => 'Stripe secret key',
  'plugins.paymethod.stripemanual.settings.secret_key.description' => 'This is the Stripe account secret key.',
  'plugins.paymethod.stripemanual.settings.public_key' => 'Stripe public key',
  'plugins.paymethod.stripemanual.settings.public_key.description' => 'This is the Stripe account public key.',
  'plugins.paymethod.stripemanual.settings.wp_site' => 'Wordpress URL',
  'plugins.paymethod.stripemanual.settings.wp_site.description' => 'Wordpress instance with Membership Pro and User payment API plugins installed.',
  'plugins.paymethod.stripemanual.settings.reg_keyword' => 'Registration type keyword',
  'plugins.paymethod.stripemanual.settings.reg_keyword.description' => 'Keyword in registration type name that indicates inclusion of membership fee.',
  'plugins.paymethod.stripemanual.settings.skip_keywords' => 'Skip keywords',
  'plugins.paymethod.stripemanual.settings.skip_keywords.description' => 'Keyword in registration type name that skips membership check. Separate multiple keywords with commas.',
  'plugins.paymethod.stripemanual.settings.discount_keyword' => 'Discounted registration type keyword',
  'plugins.paymethod.stripemanual.settings.discount_keyword.description' => 'Keyword in registration type name that indicates inclusion of discounted membership fee.',
  'plugins.paymethod.stripemanual.settings.mem_list_full' => 'Member list ID',
  'plugins.paymethod.stripemanual.settings.mem_list_full.description' => 'MembershipPro member list ID for full price members.',
  'plugins.paymethod.stripemanual.settings.mem_list_discounted' => 'Discounted member list ID',
  'plugins.paymethod.stripemanual.settings.mem_list_discounted.description' => 'MembershipPro member list ID for discounted price members.',
  'plugins.paymethod.stripemanual.settings.curlNotInstalled' => 'Warning: CURL support for PHP is not installed. Stripe payment support requires the CURL library and will not function until it is installed.',
  'plugins.paymethod.stripemanual.settings' => 'Stripe & manual Settings',
  'plugins.paymethod.stripemanual' => 'Payment',
  'plugins.paymethod.stripemanual.method' => 'Choose payment method',
  'plugins.paymethod.stripemanual.method.stripe' => 'Stripe',
  'plugins.paymethod.stripemanual.method.manual' => 'Manual',
  'plugins.paymethod.stripemanual.description' => 'The Stripe Plugin enables users, whether or not they are Stripe members, to use all major credit cards, as well as eChecks, when paying through the system. The Conference Manager will need to set up a <a href="http://www.stripe.com" target="_new">Stripe Business Account</a> to use this payment option.',
  'plugins.paymethod.stripemanual.warning' => '',
  'plugins.paymethod.stripemanual.purchase.amount' => 'Amount',
  'plugins.paymethod.stripemanual.purchase.description' => 'Description',
  'plugins.paymethod.stripemanual.error0' => 'Cannot continue with payment. Can\'t connect with membership server.',
  'plugins.paymethod.stripemanual.error1' => 'Your CAA International membership information was not found; cannot continue with the payment. Please make sure you have used the same email address for conference registration as for your <em><span title="members.caa-international.org">CAA International membership</span></em> account.<br/><br/>If you do not have a membership yet, please follow the link to the <a href="http://members.caa-international.org" target="caamember">Membership page</a>.',
  'plugins.paymethod.stripemanual.error2' => 'Cannot continue with payment. Your account registration failed in membership server.',
  'plugins.paymethod.stripemanual.error3' => 'Cannot continue with payment. Unable to receive your membership expiration date.',
  'plugins.paymethod.stripemanual.error4' => 'Cannot continue with payment. Your CAA International membership has expired or will expire before the conference you are registering. Please pay the membership fee at our <a href="https://www.members.caa-international.org/" target="caamember">membership website</a> first.',
  'plugins.paymethod.stripemanual.error5' => 'Account exists but membership not found. Contact administrator.',
  'plugins.paymethod.stripemanual.backtoreg' => 'Go back to registration page',
  'plugins.paymethod.stripemanual.settings.instructions' => 'Manual payment Instructions',
  'plugins.paymethod.stripemanual.settings.stripemanualInstructions' => 'Fee Payment Instructions for users selecting manual fee payment',
  'plugins.paymethod.stripemanual.sendNotificationOfPayment' => 'Send notification of payment',
  'plugins.paymethod.stripemanual.paymentNotification' => 'Payment Notification',
  'plugins.paymethod.stripemanual.notificationSent' => 'Payment notification sent',
  'plugins.paymethod.stripemanual.purchase.title' => 'Title',
  'plugins.paymethod.stripemanual.purchase.fee' => 'Fee',
); ?>