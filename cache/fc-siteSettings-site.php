<?php return array (
  'title' => 
  array (
    'en_US' => 'CAA Conference Registration System',
  ),
  'contactName' => 
  array (
    'en_US' => 'CAA OCS',
  ),
  'contactEmail' => 
  array (
    'en_US' => 'hembo@archaeovision.eu',
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'originalFilename' => 'header-ocs.png',
      'width' => 1003,
      'height' => 148,
      'uploadName' => 'pageHeaderTitleImage_en_US.png',
      'dateUploaded' => '2014-12-08 20:12:32',
    ),
  ),
); ?>