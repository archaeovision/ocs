<?php return array (
  'acceptSupplementaryReviewMaterials' => true,
  'closeCommentsDate' => 1335394799,
  'contactEmail' => '2012@caaconference.org',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'contactName' => 'CAA2012',
  'contactPhone' => '',
  'contactTitle' => '',
  'contributors' => 
  array (
  ),
  'copySubmissionAckAddress' => '',
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckSpecified' => false,
  'delayOpenAccessDate' => 1335308400,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'endDate' => 1333062000,
  'envelopeSender' => '',
  'locationAddress' => '',
  'locationCity' => 'Southampton',
  'locationCountry' => 'GB',
  'locationName' => 'Avenue Campus, University of Southampton',
  'metaCitations' => false,
  'metaCoverage' => true,
  'metaDiscipline' => false,
  'metaSubject' => true,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaType' => false,
  'notifyAllAuthorsOnDecision' => true,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'numWeeksPerReview' => 4,
  'numWeeksPerReviewAbsolute' => 1347231600,
  'numWeeksPerReviewRelative' => 0,
  'paymentMethodPluginName' => 'ManualPayment',
  'postAbstractsDate' => 1335308400,
  'postAccommodation' => false,
  'postCFP' => false,
  'postOverview' => false,
  'postPapersDate' => 1335308400,
  'postPayment' => false,
  'postPresentations' => false,
  'postProgram' => false,
  'postProposalSubmission' => false,
  'postSchedule' => false,
  'postScheduleDate' => 1335308400,
  'postSupporters' => false,
  'postTimeline' => false,
  'postTrackPolicies' => false,
  'previewAbstracts' => false,
  'rateReviewerOnQuality' => 0,
  'regAuthorCloseDate' => 1341961199,
  'regAuthorOpenDate' => 1319497200,
  'regReviewerCloseDate' => 1347749999,
  'regReviewerOpenDate' => 1335308400,
  'remindForInvite' => 0,
  'remindForSubmit' => 0,
  'restrictReviewerFileAccess' => 0,
  'reviewDeadlineType' => 2,
  'reviewerAccessKeysEnabled' => 0,
  'reviewMode' => 1,
  'showCFPDate' => 1335308400,
  'sponsors' => 
  array (
  ),
  'startDate' => 1332716400,
  'statCountAccept' => false,
  'statCountDecline' => false,
  'statCountRevise' => false,
  'statDaysPerReview' => false,
  'statItemsPublished' => false,
  'statNumSubmissions' => false,
  'statPeerReviewed' => false,
  'statRegisteredReaders' => false,
  'statRegisteredUsers' => false,
  'statRegistrations' => false,
  'submissionsCloseDate' => 1341961199,
  'submissionsOpenDate' => 1335308400,
  'supportEmail' => 'support@caaconference.org',
  'supportName' => 'Support',
  'supportPhone' => '',
  'acronym' => 
  array (
    'en_US' => 'CAA2012',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<span id="Paper_Length"><span id="Forms_of_Publication"><h2>Forms of Publication</h2></span><p>The CAA Proceedings will be published within one year of the conference by Amsterdam University Press (AUP) under the imprint of Pallas Publications. The CAA Proceedings consist of a printed version that will only contain the 50 papers that were ranked highest by the CAA Review College when first submitted (25th June). All accepted papers will however be available online, and as downloadable .pdf-files through the Open Access facilities provided by AUP.</p><p>The printed version of the CAA Proceedings will be sent to all members who have checked this option when registering. The CAA Proceedings can also be ordered using the Printing On Demand services of AUP.</p><span id="Review"><h2>Review</h2></span><p>Your manuscript will be reviewed by members of the CAA Review College. This will include a check for English language. At least one of the reviewers will be a native English speaker. If the quality of English means that the content of the paper cannot properly be assessed then the paper will be rejected immediately.</p><p>Reviewed papers can receive one of four possible recommendations:</p><ul><li>accept as is;</li><li>accept with minor revision (including English language, referencing and formatting);</li><li>accept with major revision (including English language, referencing and formatting);</li><li>reject.</li></ul><p>In the case of acceptance with minor or major revisions, you will receive the reviewers’ comments that will help you to improve your manuscript and a brief comment on referencing and formatting. Papers requiring major revisions must be accompanied by a brief statement of how the comments have been addressed. A rejected manuscript cannot be resubmitted.</p><p>The following fundamental selection criteria are used:</p><ul><li>the paper’s academic standard;</li><li>its consistency of content;</li><li>its clarity of style;</li><li>quality and relevance of illustrations.</li></ul><p>A full description of the review process, including the membership of the Review College, is given on the CAA website.</p><span id="Timeline_For_Publication_Caa2012"><h2>Timeline For Publication Caa2012</h2></span><p>Submission of papers <strong>1 July 2012</strong><br />Papers sent out for review <strong>1 July 2012</strong><br />Notification of acceptance/rejection <strong>10 September 2012</strong><br />Submission of revised papers <strong>4 November 2012</strong><br />Proof copies sent to authors<strong> 31 December 2012</strong><br />Authors submission of camera ready papers <strong>14 January 2013</strong><br />Papers sent to publisher <strong>20 January 2013</strong><br />Proceedings published <strong>20 March 2013</strong></p><span id="Guidelines_for_Paper_Submission"><h2>Guidelines for Paper Submission</h2><p><strong>Please read carefully</strong>; <span id="Paper_Length"><span id="Contact_Details"><strong> </strong>Due to the high number of email correspondance, we are unable to reply to queries already covered by the instructions </span></span></p><p><strong> NOTE:</strong> If you did not present in CAA2012, you are not entitled to a submission for CAA2012 proceedings</p></span><span id="Paper_Length"><h2>Paper Length</h2></span><p>If your paper is submitted as a long paper at CAA2012, your manuscript should not be longer than 5,000 words excluding abstract, key words, figures, tables and bibliography. If it is submitted as a short paper, the limit is 3,000 words. You are free to choose which type of paper to submit irrespective of your presentation type at CAA2012 (Long, Short, Poster).</p><span id="Authors_and_Affiliation"><h2>Authors and Affiliation</h2></span><p>All names and affiliations must be removed before submitting your papers</p><span id="Abstract_and_Keywords"><h2>Abstract and Keywords</h2></span><p>Provide an abstract for your paper of 150 words maximum. Provide 3-5 keywords describing the contents of your paper.</p><span id="Text"><h2>Text</h2></span><ul><li>Submit your text single-spaced, in 11-point Times New Roman Word Document. Please set paper size to A4 portrait with 2.5 cm margins on all sides.</li><li>Avoid all formatting. There are not templates to use.</li><li>Please do not indent the beginnings of paragraphs.</li><li>Provide clear headings where necessary, indicated by a number (i.e. 1 Heading Level One, 1.1 Heading Level Tw0).</li><li>The use of both bulleted and numbered lists is allowed.</li><li>Spelling should conform to British practice and follow the Oxford English Dictionary.</li><li>Submit tables, diagrams, figures etc. in separate files. These should be numbered consecutively; indicate in the text where exactly you want these to be inserted.</li><li>Submit figures and diagrams in their original format and not as a Word file.</li><li>Add a separate list for tables, diagrams, figures, graphs, maps, etc.</li><li>Create tables by using tabs (as little as possible), and not by using space.</li></ul><span id="Bibliography"><h2>Bibliography</h2></span><p>Follow the <a href="http://www.chicagomanualofstyle.org/tools_citationguide.html">Chicago style of formatting</a> (author-date system).<br />If you are using the Mendeley referencing system please make sure that you have chosen the Chicago Manual of Style (Author-Date format).<br />If you are using EndNote you should download the <a title="EndNote_Chicago Template" href="http://www.endnote.com/support/enstyledetail.asp?SORT=2&amp;PAGE=1&amp;METH=0&amp;DISC=none&amp;JOUR=chicago&amp;BSRT=none&amp;FF1=none&amp;FF2=none&amp;FF3=none&amp;CITE=none&amp;DKEY=118201124550EBA">Chicago Manual of Style 16th Edition B</a>.</p><span id="Images"><h2>Images</h2></span><p>Note that the printed version of the CAA Proceedings will have black and white images only. The online version of the CAA Proceedings can have colour versions of the images, but be sure that all of your colour images are usable when converted to greyscale.</p><p>All images must be at least 600 dpi and either 8-bit greyscale or RGB. Please use either .jpg, .tiff or .eps files. Please verify that all images are the correct size: images must be less than 16 cm wide and 24.7 cm high.</p><p>If you are submitting images, tables or any other files as <strong>supplementary to your submission (step 4)</strong>, make sure to ckeck the box \'\'Present file to reviewers (without metadata), as it will not compromise blind review\'\'. In any other case, your supplementary files cannot be made available to the reviewers and therefore your submission will not be properly reviewed.</p><span id="Copyright_Release_Form"><h2>Copyright Release Form</h2></span><p>Upon acceptance of the manuscript, all authors must fill in and sign the copyright release form. This includes co-authors.</p><p>The signed and scanned form should be attached as a supplementary file along with your <strong>final submission</strong>.</p><p><a href="http://caaconference.org/wp-content/uploads/2012/05/CAA2012_Copyright-Licence.pdf" target="_blank">Download the Copyright Form for CAA 2012</a></p><span id="Contact_Details"><h2>Contact Details</h2><p><strong>*Please Note:</strong> Due to the high number of email correspondance, we are unable to reply to queries already covered by the instructions</p></span><p><a href="mailto:proceedings@caaconference.org">proceedings@caaconference.org</a></p><p> </p><h2><strong>Start your submission below</strong></h2></span>',
  ),
  'cfpMessage' => 
  array (
    'en_US' => '<h2>Call for Papers</h2><p>The CAA proceedings consist of a printed and an electronic version. The printed version will contain the 50 papers that were ranked highest by the CAA Review College, when first submitted (25th June), while the e-version will contain all accepted papers and will be available online, and as downloadable PDF files.</p><p>If your paper is submitted as a long paper, your manuscript should not be longer than 5,000 words (excluding abstract, key words, figures, tables and bibliography); if it is submitted as a short paper, the limit is 3,000 words (again, excluding abstract, key words, figures, tables and bibliography).  You are free to choose which type of paper to submit, irrespective of your presentation type at CAA2012 (Long, Short, Poster).</p><p>Please note that if you did not present your work at CAA2012, you are not entitled to submit a paper for the CAA2012 proceedings.</p><p>The deadline for submissions has been extended until the <strong>1st of July 2012</strong> and no further extension will be given.</p><p>Any questions should be sent to <a href="mailto:proceedings@caaconference.org">proceedings@caaconference.org</a>.</p><p><span id="Paper_Length"><span id="Contact_Details"><strong>*Please Note:</strong> Due to the high number of email correspondance, we are unable to reply to queries already covered by the instructions </span></span></p><p> </p>',
  ),
  'emailSignature' => 
  array (
    'en_US' => 'Computer Applications and Quantitative Methods in Archaeology 
http://caaconference.org',
  ),
  'introduction' => 
  array (
    'en_US' => '<p class="intro">The Computer Applications and Quantitative Methods in Archaeology (CAA) 2012 conference will be hosted by the <a title="Archaeological Computing Research Group" href="http://www.southampton.ac.uk/archaeology/acrg">Archaeological Computing Research Group</a> in the <a title="Faculty of Humanities " href="http://www.southampton.ac.uk/humanities">Faculty of Humanities</a> at the University of Southampton on 26-30 March 2012</p>',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<p>In order to make your and our task easier, we provide you with the following guidelines in reviewing.</p> <ol><li>The criteria that you should evaluate are</li></ol> <ul><li>the paper’s academic standard;</li><li>its consistency of content;</li><li>its clarity of style;</li><li>quality and relevance of illustrations.</li></ul> <p>For each of these criteria you should provide one of three possible assessments:</p> <ul><li>good; </li><li>in need of improvement; </li><li>insufficient. </li></ul> <p>These will help you to arrive at the final assessments of the paper, which can be</p> <ul><li>accept as is;</li><li>revisions required;</li><li>reject.</li></ul> <p>In the case of assessment of criteria in the categories ‘in need of improvement’ or ‘insufficient’ you should clearly indicate in <em>Comments to the Author</em> what needs to be changed in each of these categories in order to get these aspects of the paper to the desired level.<br /> <br /> <span style="text-decoration: underline;">Please note</span> that you should also do this in the case that you reject a paper, since the author deserves to know why and since the paper will be reviewed by at least one other colleague, who may or may not agree with your judgment.</p><p>Since we only have the possibility to publish a maximum of 50 papers in print, we will select those on the basis of your assessments above. For this reason you should also provide a rating from 1-10 considering the overall quality of the paper. This rating is only for internal consumption and will not become available to the authors.</p><p>The submission of reviews is done through the OCS system. You should be a registered user of OCS; if this is not the case yet, please register as soon as possible. OCS will not allow you to upload attachments with comments, so please enter all comments in the appropriate form.</p><p>Please use the field <em>Comments for the Director</em> to provide any additional confidential details.</p> <p>The final submission date for your review is the <span style="text-decoration: underline;">10th of September 2012.</span></p><p> </p><p><strong>Step-by-Step Guidance for Reviewers in the OCS System</strong></p> <p><strong> </strong></p> <p><strong>a.</strong> Log in to your account with your registered username and password</p> <p><strong>b.</strong> Under CAA2012 go to Reviewer and check the active submissions</p> <p><strong>c.</strong> Choose which of these you want first to review</p> <p><strong>d.</strong> In the first section you will find the information needed (title, conference track, abstract, submissions’ directors and metadata).</p> <p><strong>e.</strong> In the second section it is the review schedule. Please note the review due date. No extension can be given.</p> <p><strong>f.</strong> Review Steps</p> <p><strong> 1.</strong> Accept the review (<strong>will do the review</strong>). Optionally you can notify the submission’s director that you accepted. Please make sure that in case you are <strong>unable to do the review</strong> to notify as soon as possible the directors.</p> <p><strong> 2.</strong> Check the guidelines below</p> <p><strong> 3.</strong> Here you can find the manuscript and any supplementary files</p> <p><strong>4.</strong> Click the bubble to open the review form. Everything should be in the form. <strong>Do not </strong>upload any files in Step 5</p> <p><strong> 5.</strong> Skip</p> <p><strong>6.</strong> Choose your recommendation. This should be the same with the recommendation chosen at the end of the review form.</p> <p> </p> <p><strong>Review Form </strong></p> <p>1. You should provide one of three possible assessments for the academic standard of the paper, the consistency of its content, the clarity of its style, and the quality and relevance of illustrations:</p> <ul><li>Good; </li><li>In need of      improvement; </li><li>Insufficient. </li></ul> <p>In cases that you pick the last two options, you must clarify in the <em>Comments to the Authors</em> section what should be done in order to improve this aspect of the paper.</p> <p>2. You should classify the paper in one of the themes of the conference. This might not necessarily be the same theme that the paper was presented in during the conference.</p> <p>3. Please assess the length of the paper. Long papers are approximately 5,000 words and short papers approximately 3,000 words excluding references, tables and bibliography. Has the paper the right length or it should be turned into the other format (LongàShort / Shortà Long)?</p> <p>4. You should make sure that the paper complies with all the statements (check the relevant boxes)</p> <p>a. The paper includes an abstract and keywords</p> <p>b. Text has been formatted according to the guidelines send to all contributors along with the call for papers</p> <p>c. Referencing follows the Chicago style of formatting (author-date system)</p> <p>d. The level of English is good. In any other case suggestions for improvement should be included in the \'Comments to the Authors\' section.</p> <p>5. Please state if the paper needs any revisions. If you choose one of the first two options you must clarify in the <em>Comments to the Authors</em> section what should be done in order to improve this aspect of the paper.</p> <p>6. Write down in detail your comments to the authors in order to improve the paper. Please provide as much information as possible, especially in cases that you consider than an aspect of paper is insufficient. Incomplete reviews will be returned to the reviewers.</p> <p>7. Write down any confidential comments to the Directors</p> <p>8. Please provide an overall rating for the paper. This will not become available to the author, but will help us, along with the rest of the assessment, in choosing the best papers that will go to the printed proceedings.</p> <p>9. Choose your final decision for the paper.</p>',
  ),
  'reviewPolicy' => 
  array (
    'en_US' => '<p>Your manuscript will be reviewed by members of the CAA Review College. This will include a check for English language. At least one of the reviewers will be a native English speaker. If the quality of English means that the content of the paper cannot properly be assessed then the paper will be rejected immediately.</p> <p> </p> <p>Reviewed papers can receive one of three possible recommendations:</p> <ul><li> accept as is;</li><li> accept with revisions (minor or major revision) (including English language, referencing and formatting);</li><li> reject.</li></ul> <p> </p> <p>In the case of acceptance with minor or major revisions, you will receive the reviewers’ comments that will help you to improve your manuscript and a brief comment on referencing and formatting. Papers requiring major revisions <strong>must</strong> be accompanied by a brief statement of how the comments have been addressed. <strong>A rejected manuscript cannot be resubmitted.</strong></p> <p> </p> <p>The following fundamental selection criteria are used:</p> <ul><li> the paper’s academic standard;</li><li> its consistency of content;</li><li> its clarity of style;</li><li> quality and relevance of illustrations.</li></ul>',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'The submission has not been previously published, nor is it before another conference for consideration (or an explanation has been provided in Comments to the Director).',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'The submission file is in Microsoft Word file format.',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the <a href="/ocs/index.php?conference=caa&amp;schedConf=2012&amp;page=schedConf&amp;op=cfp">Author Guidelines</a>.<br />',
      ),
      3 => 
      array (
        'order' => '4',
        'content' => '<span>Make sure authors\' names, affiliations or any other credentials are removed from submission.</span>',
      ),
      4 => 
      array (
        'order' => '5',
        'content' => 'If you are submitting images, tables or any other files as supplementary to your submission (step 4), make sure to ckeck the box \'\'Present file to reviewers (without metadata), as it will not compromise blind review\'\'. In any other case, your supplementary files cannot be made available to the reviewers and therefore your submission will not be properly reviewed.  <br />',
      ),
      5 => 
      array (
        'order' => '6',
        'content' => '<p><span>You should make sure that all Microsoft Office documents (including Supplementary Files) do not have any personal information. To do this follow the process below: Go to the Microsoft Office Word Home Icon --&gt; Prepare --&gt; Inspect Document --&gt; Check Document properties and personal information --&gt; Press Inspect --&gt; Choose <strong>Remove All</strong> for Document properties and personal information.</span></p>',
      ),
    ),
  ),
  'title' => 
  array (
    'en_US' => 'CAA 2012',
  ),
); ?>