<?php return array (
  'numWeeksPerReview' => 4,
  'reviewDeadlineType' => 1,
  'emailSignature' => 
  array (
    'en_US' => '----
CAA Conference 2017
http://caaconference.org/',
  ),
  'envelopeSender' => '',
  'sponsors' => 
  array (
  ),
  'paymentMethodPluginName' => 'ManualPayment',
  'title' => 
  array (
    'en_US' => 'CAA 2017',
  ),
  'acronym' => 
  array (
    'en_US' => 'CAA2017',
  ),
  'locationName' => 'Georgia State University',
  'locationAddress' => '',
  'locationCity' => 'Atlanta',
  'locationCountry' => 'US',
  'contactName' => 'Jeffrey Glover',
  'contactTitle' => 'Associate Professor',
  'contactAffiliation' => 
  array (
    'en_US' => 'Department of Anthropology
College of Arts and Sciences
Georgia State University',
  ),
  'contactEmail' => 'caa2017@caaconference.org',
  'contactPhone' => '',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'supportName' => 'Jeffrey Glover',
  'supportEmail' => 'caa2017@caaconference.org',
  'supportPhone' => '',
  'contributors' => 
  array (
  ),
  'reviewMode' => 0,
  'previewAbstracts' => false,
  'acceptSupplementaryReviewMaterials' => false,
  'copySubmissionAckPrimaryContact' => true,
  'copySubmissionAckSpecified' => false,
  'copySubmissionAckAddress' => '',
  'metaDiscipline' => false,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaSubject' => true,
  'metaCoverage' => false,
  'metaType' => false,
  'metaCitations' => false,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'remindForInvite' => 1,
  'remindForSubmit' => 1,
  'rateReviewerOnQuality' => 0,
  'restrictReviewerFileAccess' => 1,
  'reviewerAccessKeysEnabled' => 0,
  'numDaysBeforeInviteReminder' => 3,
  'numDaysBeforeSubmitReminder' => 2,
  'numWeeksPerReviewRelative' => 1,
  'numWeeksPerReviewAbsolute' => 1500246000,
  'notifyAllAuthorsOnDecision' => true,
  'postTimeline' => false,
  'postOverview' => false,
  'postCFP' => false,
  'postProposalSubmission' => false,
  'postTrackPolicies' => false,
  'postProgram' => false,
  'postPresentations' => false,
  'postAccommodation' => false,
  'postSupporters' => false,
  'postPayment' => false,
  'startDate' => 1489449600,
  'endDate' => 1489622400,
  'regAuthorOpenDate' => 1465945200,
  'regAuthorCloseDate' => 1485734399,
  'showCFPDate' => 1465945200,
  'submissionsOpenDate' => 1473634800,
  'submissionsCloseDate' => 1485734399,
  'regReviewerOpenDate' => 1465945200,
  'regReviewerCloseDate' => 1484611199,
  'postAbstractsDate' => 1465167600,
  'postScheduleDate' => 1465167600,
  'postPapersDate' => 1465167600,
  'delayOpenAccessDate' => 1465167600,
  'closeCommentsDate' => 1465340399,
  'boardEnabled' => false,
  'introduction' => 
  array (
    'en_US' => '<img style="width: 100%;" src="http://sites.caa-international.org/caa2017/wp-content/uploads/sites/18/2016/06/cropped-CAA2017_Banner_noimage-1.jpg" width="100%" alt="" />

The 45th annual CAA conference will bring together scholars from across the globe to share their cutting edge research from a diverse range of fields in a focused, but informal, setting.  One thing that the CAA prides itself on is a strong sense of community, and we hope to continue to grow that community by welcoming new participants this year.  This is only the 3rd time the conference has been held in the United States, and we are excited to have old and new members join us in Atlanta this coming spring.',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p><strong>ABSTRACT LENGTH</strong></p> <p>Your paper or poster abstract should not be longer than 250 words including title, affiliations and key words.</p><p><strong>AUTHORS AND AFFILIATION</strong></p> <p>Provide the full names and affiliations of all authors, including e-mail addresses. Please indicate the name of the corresponding author.</p><p><strong>KEYWORDS</strong></p> <p>Provide 3-5 keywords describing your session.</p> <p><strong>LANGUAGE</strong></p><p><strong> </strong>The official language of the conference is English. Spelling should conform to British practice and follow the Oxford English Dictionary.</p> <p><strong>START THE SUBMISSION PROCESS</strong></p>',
  ),
  'cfpMessage' => 
  array (
    'en_US' => 'Please submit your paper or poster abstract in the OCS system.',
  ),
  'metaSubjectExamples' => 
  array (
    'en_US' => 'E.g., GIS; Geophysics; 3D Visualization; Data Management',
  ),
); ?>