<?php return array (
  'acceptSupplementaryReviewMaterials' => false,
  'boardEnabled' => false,
  'closeCommentsDate' => 1334876399,
  'contactEmail' => 'arianna.traviglia@mq.edu.au',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'contactName' => 'Dr Arianna Traviglia',
  'contactPhone' => '',
  'contactTitle' => '',
  'contributors' => 
  array (
    0 => 
    array (
      'name' => 'Squire Sanders',
      'url' => 'www.squiresanders.com',
    ),
  ),
  'copySubmissionAckAddress' => 'ian.johnson@sydney.edu.au',
  'copySubmissionAckPrimaryContact' => true,
  'copySubmissionAckSpecified' => false,
  'delayOpenAccessDate' => 1334790000,
  'enableAuthorSelfArchive' => false,
  'enableOpenAccessNotification' => false,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'enableRegistrationExpiryReminderAfterMonths' => false,
  'enableRegistrationExpiryReminderAfterWeeks' => false,
  'enableRegistrationExpiryReminderBeforeMonths' => false,
  'enableRegistrationExpiryReminderBeforeWeeks' => false,
  'endDate' => 1364428800,
  'envelopeSender' => '',
  'locationAddress' => '<p><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-bidi-font-size: 7.5pt;">35 Stirling Highway <br /> CRAWLEY WA 6009</span></p>',
  'locationCity' => 'Perth, Western Australia',
  'locationCountry' => 'AU',
  'locationName' => 'The University Club of Western Australia',
  'metaCitations' => false,
  'metaCoverage' => true,
  'metaDiscipline' => false,
  'metaSubject' => true,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaType' => true,
  'notifyAllAuthorsOnDecision' => true,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 1,
  'numMonthsAfterRegistrationExpiryReminder' => 0,
  'numMonthsBeforeRegistrationExpiryReminder' => 0,
  'numWeeksAfterRegistrationExpiryReminder' => 0,
  'numWeeksBeforeRegistrationExpiryReminder' => 0,
  'numWeeksPerReview' => 4,
  'numWeeksPerReviewAbsolute' => 1378594800,
  'numWeeksPerReviewRelative' => 4,
  'paymentMethodPluginName' => 'OneStopPayments',
  'postAbstractsDate' => 1334790000,
  'postAccommodation' => false,
  'postCFP' => false,
  'postOverview' => true,
  'postPapersDate' => 1334790000,
  'postPayment' => false,
  'postPresentations' => false,
  'postProgram' => false,
  'postProposalSubmission' => false,
  'postSchedule' => false,
  'postScheduleDate' => 1334790000,
  'postSupporters' => false,
  'postTimeline' => false,
  'postTrackPolicies' => false,
  'previewAbstracts' => false,
  'rateReviewerOnQuality' => 1,
  'regAuthorCloseDate' => 1372028399,
  'regAuthorOpenDate' => 1368745200,
  'registrationEmail' => 'registrations@caa2013.org',
  'registrationFax' => '',
  'registrationMailingAddress' => '',
  'registrationName' => 'Registration team',
  'registrationPhone' => '+61298508889',
  'regReviewerCloseDate' => 1388534399,
  'regReviewerOpenDate' => 1372028400,
  'remindForInvite' => 0,
  'remindForSubmit' => 1,
  'restrictReviewerFileAccess' => 1,
  'reviewDeadlineType' => 2,
  'reviewerAccessKeysEnabled' => 1,
  'reviewMode' => 2,
  'showCFPDate' => 1368745200,
  'sponsors' => 
  array (
    0 => 
    array (
      'institution' => 'The University of Western Australia',
      'address' => '<img src="/ocs/public/site/images/taa94/uwa-logo.gif" alt="" />',
      'url' => 'www.uwa.edu.au',
    ),
    1 => 
    array (
      'institution' => 'Perth Convention Bureau',
      'address' => '<img src="/ocs/public/site/images/taa94/PCB_logo.png" alt="" width="553" height="79" />',
      'url' => 'www.pcb.com.au',
    ),
  ),
  'startDate' => 1364169600,
  'statCountAccept' => false,
  'statCountDecline' => false,
  'statCountRevise' => false,
  'statDaysPerReview' => false,
  'statisticsTrackIds' => 
  array (
  ),
  'statItemsPublished' => false,
  'statNumSubmissions' => false,
  'statPeerReviewed' => false,
  'statRegisteredReaders' => false,
  'statRegisteredUsers' => false,
  'statRegistrations' => false,
  'submissionsCloseDate' => 1372028399,
  'submissionsOpenDate' => 1369263600,
  'supportEmail' => 'info@caa2013.org',
  'supportName' => 'the CAA2013 Support',
  'supportPhone' => '',
  'acronym' => 
  array (
    'en_US' => 'CAA2013',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p><strong>PAPER LENGTH</strong></p><p>If your paper is submitted as a long paper, your manuscript should not be longer than 5,000 words excluding abstract, key words, figures, tables and bibliography. If it is submitted as a short paper, the limit is 3,000 words. You are free to choose which type of paper to submit irrespective of your presentation type at CAA2013.</p><p><br /><strong>AUTHORS AND AFFILIATION</strong></p><p>Provide the full names and affiliations of all authors, including e-mail addresses. Please indicate the name of the corresponding author.</p><p><br /><strong>ABSTRACT AND KEYWORDS</strong></p><p>Provide an abstract for your paper of 150 words maximum. Provide 3-5 keywords describing the contents of your paper.</p><p><strong>TEXT</strong></p><ul><li>Submit your text single-spaced, in 11-point Times New Roman. Please set paper size to A4 portrait with 2.5 cm margins on all sides. </li><li>Avoid all formatting. </li><li>Please do not indent the beginnings of paragraphs. </li><li>Provide clear headings where necessary, indicated by a number (i.e. 1 Heading Level One, 1.1 Heading Level Two). </li><li>The use of both bulleted and numbered lists is allowed.</li><li>Spelling should conform to British practice and follow the Oxford English Dictionary.</li><li>Submit tables, diagrams, figures etc. in separate files. These should be numbered consecutively (see below the FILE NAMING section); indicate in the text where exactly you want these to be inserted.</li><li>Submit figures and diagrams in their original format and not as a Word file.</li><li>Add a separate list for tables, diagrams, figures, graphs, maps, etc.</li><li>Create tables by using tabs (as little as possible), and not by using space.</li></ul><p><br /><strong>BIBLIOGRAPHY</strong></p><p>Follow the Chicago style of formatting, author-date system: <a title="Chicago style " href="http://www.chicagomanualofstyle.org/tools_citationguide.html" target="_blank">http://www.chicagomanualofstyle.org/tools_citationguide.html</a></p><p>The Chicago style requires you to use full first and last names for authors; since this is highly impractical, please use initials and last names only.</p><p>If you are using the Mendeley referencing system please make sure that you have chosen the Chicago Manual of Style (Author-Date format) template. In EndNote the style is called Chicago Manual of Style 16th Edition B (not A). It can be downloaded from the EndNote website: <a title="Endnote" href="http://endnote.com/downloads/style/chicago-manual-style-16th-edition-b" target="_blank">http://endnote.com/downloads/style/chicago-manual-style-16th-edition-b</a></p><p><strong>Please note: </strong>it is <span style="text-decoration: underline;">essential </span>that you submit the references in the correct format. Manuscripts that do not follow the correct referencing style will be sent back to authors for revision and might risk missing the final deadline and consequently exclusion from publication.<strong> </strong></p><p> </p><p><strong>IMAGES</strong></p><p>Note that the printed version of the CAA Proceedings will have black and white images only. The online version of the CAA Proceedings can have colour versions of the images.  If your paper has been accepted for publication on the printed proceedings, and you are submitting colour images, please provide greyscale versions as well. Make sure that all of your colour images are usable when converted to greyscale.</p><p>All images must be at least 600 dpi and either 8-bit greyscale or RGB. Please use either .jpg, .tiff or .eps files. Verify that all images are the correct size: images must be less than 16 cm wide and 24.7 cm high.</p><p> </p><p><strong>FILE NAMING</strong></p> <p>- Please <strong>name your manuscript</strong> Word file as: surname of first author, first 2 meaningful words of the title (shortened, if needed) excluding articles, and number of submission in the editorial process, all  separated by underscore, e.g. \'Traviglia_ArcheolSurvey_1\' where \'1\' indicates the first, initial submission; when you will resubmit your amended manuscript after the review that number will have to be changed in \'_2\').</p> <p>- Please, similarly <strong>name your images, tables, diagrams etc</strong>: surname of first author, first 2 meaningful words of the title, file content abbreviation with file consecutive number, and number of submission in the editorial process  e.g. \'Traviglia_ArcheolSurvey _fig1-1\' or \'Traviglia_ArcheolSurvey _tab1-1\'.</p> <p><strong>Please note: </strong>it is <span style="text-decoration: underline;">essential </span>that you name the files according to these instructions. Paper proposals that do not follow the correct naming style and zipping procedure will be sent back to authors for revision and might risk missing the final deadline and consequently exclusion from publication.</p><p><strong> </strong></p> <p> </p> <p><strong>HOW  TO SUBMIT</strong></p> <p>Please follow these instructions carefully. It is <span style="text-decoration: underline;">essential</span> that you submit all your files in a single zip file (abstract, paper, images, tables etc. and -later on- copyright release form). Please <span style="text-decoration: underline;">make sure</span> you first create a folder including all the files your want to submit and then you zip it.</p> <p><strong>Name the folder as</strong>: surname of first author, first 2 meaningful words of the title (shortened, if needed) excluding articles, and number of submission in the editorial process, all separated by underscores, e.g. \'Traviglia_ArcheolSurvey_1\' and then maintain this naming when you zip the folder.</p> <p>Only in this way the original naming, essential for quick paper identification, will be  saved through the OCS upload procedure.</p><p> </p><p><strong>COPYRIGHT RELEASE FORM</strong></p><p>Upon acceptance of the manuscript, all authors must fill in and sign the copyright release form. This includes co-authors.</p><p> </p><p><strong>START THE SUBMISSION PROCESS</strong></p>',
  ),
  'cfpMessage' => 
  array (
    'en_US' => '<p><strong>CALL FOR PAPERS</strong></p><p>The CAA 2013 Organising Committee invites manuscripts of papers and posters presented during the 41st Computer Applications and Quantitative Methods in Archaeology Conference CAA 2013 Perth Across Space and Time, held in Perth, Western Australia 25-28th March 2013.</p><p>The CAA Proceedings will be published within one year of the conference by Amsterdam University Press (AUP) under the imprint of Pallas Publications. The CAA Proceedings consist of a printed version that will only contain the 50 papers that were ranked highest by the CAA Review College when first submitted. All accepted papers will however be available online, and as downloadable .pdf-files through the Open Access facilities provided by AUP.</p><p>You can submit your manuscript as <strong>Long Paper</strong> or <strong>Short Paper</strong>. You are free to choose which type of paper to submit <span style="text-decoration: underline;">irrespective of your presentation type at CAA2013</span>.</p><p>If your paper is submitted as a <strong>long paper</strong>, your manuscript should not be longer than <strong>5,000 </strong>words excluding abstract, key words, figures, tables and bibliography. If it is submitted as a <strong>short paper,</strong> the limit is<strong> 3,000</strong> words.</p><p><strong>Please note:</strong> if you or your co-authors did not present a paper or poster in CAA2013, you are not entitled to a submission for CAA2013 proceedings.</p><p><strong>Manuscript proposals are due by 23 June 2013 (UTC).</strong></p><p><strong>TIMELINE FOR PUBLICATION</strong></p><ul><li>Submission of papers <span style="white-space: pre;"> </span>23 June 2013</li><li>Papers sent out for review <span style="white-space: pre;"> </span>30 June 2013</li><li>Notification of acceptance/rejection <span style="white-space: pre;"> </span>6 September 2013</li><li>Submission of revised papers<span style="white-space: pre;"> </span>27 October 2013</li><li>Proof copies sent to authors <span style="white-space: pre;"> </span>22 December 2013</li><li>Authors submission of camera ready papers <span style="white-space: pre;"> </span>10 January 2014</li><li>Papers sent to publisher <span style="white-space: pre;"> </span>19 January 2014</li><li>Proceedings published <span style="white-space: pre;"> </span>15 March 2014.</li></ul><p>The complete 2013 <strong>Authors\' instructions can be downloaded</strong> from the following link: <strong><a title="Authors instructions" href="http://caa2013.org/drupal/documents/CAA2013-Authors-instructions.pdf" target="_blank">http://caa2013.org/drupal/documents/CAA2013-Authors-instructions.pdf</a> </strong>. Please read them carefully.</p><p><strong>Illustrated submission instructions </strong>are available at: <strong><a href="http://caa2013.org/drupal/documents/CAA2013Submission_instructions.pdf">http://caa2013.org/drupal/documents/CAA2013Submission_instructions.pdf</a></strong>. Please read them carefully before submission to avoid common uploading issues.</p><p><span style="font-family: Times New Roman; font-size: small;"> </span>If you have any questions about the Guidelines, please contact the CAA Editorial Board: <a title="Mail CAA Editorial Board" href="mailto:proceedings@caaconference.org" target="_self">proceedings@caaconference.org</a>.</p><p>If you have problems with the submission please contact <a title="Mail Submissions" href="mailto:submissions@caa2013.org" target="_blank">submissions@caa2013.org</a>.</p><p><strong>Please note</strong>: due to the high number of email correspondence, we are unable to reply to queries already covered by the instructions.</p><p>Start to submit your proposal <strong>using the link available on the bottom of this page</strong>.</p>',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'Macquarie University',
  ),
  'emailSignature' => 
  array (
    'en_US' => '___________________________

41st Computer Applications in Archaeology and Quantitative Methods in Archaeology Conference
CAA 2013 Perth Across Space and Time
25-28 March 2013 Perth, Western Australia

Conference web site: http://caa2013.org
FB: http://www.facebook.com/CAAPerth2013 
Twitter: @CAA2013Perth
Info: info@caa2013.org
Submissions: submissions@caa2013.org
Chair: chair@caa2013.org',
  ),
  'introduction' => 
  array (
    'en_US' => '<span style="font-family: Times New Roman; font-size: small;"><strong style="font-size: small; font-family: \'Times New Roman\';"><span style="color: black; font-family: &quot;Verdana&quot;,&quot;sans-serif&quot;; font-size: 9pt;"> </span></strong><span style="font-size: small;"> </span><p class="MsoNormal" style="margin: 0cm 0cm 0pt; line-height: normal;"><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-fareast-language: EN-AU;">The 2013 CAA conference “Across Space and Time“ was held in Perth, Western Australia, at the University Club of Western Australia.</span></p><p class="MsoNormal" style="margin: 0cm 0cm 0pt; line-height: normal;"><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; font-size: 12pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: &quot;Times New Roman&quot;; mso-fareast-language: EN-AU;">For general information about the conference see the conference web site at <strong><a href="http://caa2013.org/" target="_blank">http://caa2013.org</a>.</strong></span></p><p class="MsoNormal" style="margin: 0cm 0cm 0pt; line-height: normal;"> </p></span><strong> </strong>',
  ),
  'metaCoverageChronExamples' => 
  array (
    'en_US' => '(E.g., Early Middle-ages; Iron age). Separate terms with a semicolon. Optional.',
  ),
  'metaCoverageGeoExamples' => 
  array (
    'en_US' => '(E.g., Iberian Peninsula; Egypt). Separate terms with a semicolon. Optional.',
  ),
  'metaCoverageResearchSampleExamples' => 
  array (
    'en_US' => '(E.g., Age; Gender; Ethnicity; etc.). Separate terms with a semicolon. Optional.',
  ),
  'metaDisciplineExamples' => 
  array (
    'en_US' => 'eg. Archaeology, Geography, GIS, Information Science, Education, Learning Technologies',
  ),
  'metaSubjectExamples' => 
  array (
    'en_US' => '(E.g., Semantic Web; surface modelling; predictive modelling). Separate terms with a semicolon.',
  ),
  'metaTypeExamples' => 
  array (
    'en_US' => '(E.g., Field recording, network visualisation, database management, ontological analysis). Separate terms with a semicolon. Optional.',
  ),
  'overview' => 
  array (
    'en_US' => '<p><span style="font-family: Arial;"><span style="font-family: Times New Roman; font-size: small;"> </span><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU;"><span style="font-size: small;">CAA is the premier international conference for all aspects of computing, quantitative methods and digital applications in Archaeology. With a history going back to 1972. CAA encourages participation from scholars, specialists and experts in the fields of archaeology, history, cultural heritage, digital scholarship, GIS, mathematics, semantic web, informatics and members of other disciplines that complement and extend the interests of CAA. </span></span></span><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU;"><br /><span style="font-size: small;">The main themes of the CAA 2013 Perth conference included:</span></span></p><ul><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">Field and laboratory data recording</span></li><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">Data management, analysis and the semantic web</span></li><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">3D modelling, visualisation and simulations</span></li><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">Spatiotemporal modelling and GIS</span></li><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">Remote sensing (long and short range)</span></li><li><span style="font-size: small; font-family: \'Century Gothic\', sans-serif;">Cultural heritage management and interpretation.</span></li></ul><p> </p><p><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU;"><span style="font-size: small;">If you <strong>do not have a login</strong>, please create one at the ACCOUNT tab (located at the top of the page) or click <a title="New account" href="/ocs/index.php?conference=caa&amp;schedConf=index&amp;page=user&amp;op=account" target="_self">here</a> and select CAA2013. <span>In the \'Create account as\' section please make sure you select \'Author\'.</span> </span></span></p><p><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU;"><span style="font-size: small;">If you <strong>previously logged</strong> into the CAA Open Conference System (OCS), you can <a title="Log in" href="/ocs/index.php?conference=caa&amp;schedConf=index&amp;page=login" target="_self">log</a> with the same user name and password. Should you have log in problems, use the ‘<a title="Lost password" href="/ocs/index.php?conference=caa&amp;schedConf=index&amp;page=login&amp;op=lostPassword" target="_self">Forgot your password?</a>’ link on the LOG IN tab to recover your user name and a new password.<br /></span></span><span style="font-family: \'Century Gothic\', sans-serif; font-size: small;"> </span></p><p> </p><p><span style="font-family: Times New Roman; font-size: small;"> </span></p><p><span style="color: black; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU;"><span style="font-size: small;"><span style="color: black; line-height: 115%; font-family: &quot;Century Gothic&quot;,&quot;sans-serif&quot;; font-size: 11pt; mso-fareast-font-family: &quot;Times New Roman&quot;; mso-bidi-font-family: Arial; mso-fareast-language: EN-AU; mso-ansi-language: EN-AU; mso-bidi-language: AR-SA;"> </span></span></span></p><p><span style="font-family: Times New Roman; font-size: small;"> </span></p>',
  ),
  'registrationAdditionalInformation' => 
  array (
    'en_US' => '<p>Early Bird registration runs until February 7th 2013 (11:59pm GMT).</p><p>Standard fees apply from February 8th 2013 (12:00am GMT).</p><p>Registration closes on March 22nd 2013 (11:59pm GMT).</p><p><strong>Please note</strong>: registration fee and option credit card payment can only be processed <strong>once</strong>. Please make sure you chose all the options you are interested in before completing the payment.</p>',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<p>The papers will be reviewed by members of the CAA 2013 Review College.</p> <p>The role of the reviewer is twofold. Firstly, it consists of assisting the CAA 2013 Scientific Committee in assessing the quality of proposals submitted. Reviews do not altogether determine which proposals will be accepted or rejected, but rather provide expert information which Scientific Committee use in making its decisions. Each paper must be reviewed by at least three people.</p> <p>The second major role for reviewers is to provide helpful, constructive feedback to authors, which can strengthen both the quality of the conference Proceedings and the collegiality and intellectual rigor of the CAA authors.</p> <p>The following selection criteria will be used by the reviewers:</p><ul><li>the paper’s academic standard (the extent to which the paper is original, technically excellent, critical);</li><li>its consistency of content;</li><li>its clarity of style;</li><li>quality and relevance of illustrations.</li></ul><p>A <strong>good review</strong> should suggest concrete ways in which the proposal may be strengthened. This feedback is important whether the reviewer is recommending that the paper be rejected or accepted. If the former, it will enable the author to revise his/her paper submission and strengthen it or to submit a stronger proposal next year; if the latter, it will result in a stronger paper being submitted. In either case, constructive criticism projects collegiality and an interest in others’ work. It is <strong>essential that the reviewers be uncompromisingly professional and courteous </strong>in reviewing all submissions, even when their paper assessment is not positive. Comments which are purely negative should be addressed solely to the Editorial Panel.</p> <p>The review procedure will include a <strong>check for English language</strong>. To this end, at least one of the reviewers will be a native English speaker. If the quality of English prevents proper assessment then the paper can be rejected (this will have to be indicated as the reason for rejection in the Comments to the Authors box). If however reviewers feel that the content is expressed well enough for them to evaluate the submission then they can accept the submission but add a note in the Comments to the Authors box indicating that acceptance is subject to revision of the paper’s English.</p> <p>Reviewed papers can receive one of following possible recommendations:</p> <ul><li>accept as is: the paper is of the appropriate quality and fits with the aims of the conference as a whole.</li><li>accept with revisions (minor or major revision) (including English language, referencing and formatting): the paper is of sufficient level but requires (minor or major) revision. The reviewers will have to provide the authors with comments that will help them to improve their manuscript and, if needed, a brief comment on referencing and formatting. Authors of papers requiring major revisions will have to resubmit their papers accompanied by a brief statement of how the reviewers’ comments have been addressed.</li><li>reject: the paper is not of the appropriate quality or it does not fit to the conference as a whole. Rejected manuscript cannot be resubmitted.</li></ul> <p> </p> <p> </p> <p> </p><p> </p> <p>[We gratefully acknowledge use of extracts from the Digital Humanities conference guide for reviewers]</p>',
  ),
  'reviewPolicy' => 
  array (
    'en_US' => '<p>The papers will be reviewed by members of the CAA 2013 Review College. Each paper must be reviewed by at least three people.</p> <p>The following selection criteria will be used by the reviewers:</p> <ul><li>the paper’s academic standard (the extent to which the paper is original, technically excellent, critical);</li><li>its consistency of content;</li><li>its clarity of style;</li><li>quality and relevance of illustrations.</li></ul> <p>The review procedure will include a check for English language. To this end, at least one of the reviewers will be a native English speaker. If the quality of English prevents proper assessment then the paper can be rejected (this will have to be indicated as the reason for rejection in the Comments to the Authors box).</p> <p>Reviewed papers can receive one of following possible recommendations:</p> <ul><li>accept as is: the paper is of the appropriate quality and fits with the aims of the conference as a whole.</li><li>accept with revisions (minor or major revision) (including English language, referencing and formatting): the paper is of sufficient level but requires (minor or major) revision. The reviewers will have to provide the authors with comments that will help them to improve their manuscript.Authors of papers requiring major revisions will have to resubmit their papers accompanied by a brief statement of how the reviewers’ comments have been addressed.</li><li>reject: the paper is not of the appropriate quality or it does not fit to the conference as a whole. Rejected manuscript cannot be resubmitted.</li></ul>',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'The submission has not been previously published, nor it is before another conference for consideration (or an explanation has been provided in Comments to the Director).',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'The manuscript file is in Microsoft Word file format.',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'All the submitted files are named as for the Author Guidelines.',
      ),
      3 => 
      array (
        'order' => '4',
        'content' => 'All the submitted files are included in a .zip file named as for the Author Guidelines.',
      ),
    ),
  ),
  'title' => 
  array (
    'en_US' => 'CAA 2013',
  ),
); ?>