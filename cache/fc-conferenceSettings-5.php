<?php return array (
  'commentsAllowAnonymous' => false,
  'commentsRequireRegistration' => false,
  'conferenceEventLog' => true,
  'conferenceTheme' => '',
  'contactEmail' => 'james@archaeovision.eu',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'contactName' => 'James Miles',
  'contactPhone' => '',
  'contactTitle' => '',
  'copyrightNoticeAgree' => false,
  'enableAnnouncements' => false,
  'enableAnnouncementsHomepage' => false,
  'enableComments' => false,
  'itemsPerPage' => 25,
  'numAnnouncementsHomepage' => 0,
  'numPageLinks' => 10,
  'paperAccess' => 0,
  'paperEmailLog' => true,
  'paperEventLog' => true,
  'paymentMethodPluginName' => 'ManualPayment',
  'postCreativeCommons' => false,
  'restrictPaperAccess' => false,
  'rtAbstract' => true,
  'rtAddComment' => true,
  'rtAuthorBio' => true,
  'rtCaptureCite' => true,
  'rtDefineTerms' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'rtPrinterFriendly' => true,
  'rtSupplementaryFiles' => true,
  'rtViewMetadata' => true,
  'schedConfRedirect' => 7,
  'supportedLocales' => 
  array (
    0 => 'en_US',
  ),
  'archiveAccessPolicy' => 
  array (
    'en_US' => 'The presentations that make up the current and archived conferences on this site have been made open access and are freely available for viewing, for the benefit of authors and interested readers.',
  ),
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => 'Authors are permitted to post their papers on personal or institutional websites prior to and after publication by this conference (while providing the bibliographic details of that publication).',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'Archeovision',
  ),
  'copyrightNotice' => 
  array (
    'en_US' => 'Authors who submit to this conference agree to the following terms:<br /> <strong>a)</strong> Authors retain copyright over their work, while allowing the conference to place this unpublished work under a <a href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Attribution License</a>, which allows others to freely access, use, and share the work, with an acknowledgement of the work\'s authorship and its initial presentation at this conference.<br /> <strong>b)</strong> Authors are able to waive the terms of the CC license and enter into separate, additional contractual arrangements for the non-exclusive distribution and subsequent publication of this work (e.g., publish a revised version in a journal, post it to an institutional repository or publish it in a book), with an acknowledgement of its initial presentation at this conference.<br /> <strong>c)</strong> In addition, authors are encouraged to post and share their work online (e.g., in institutional repositories or on their website) at any point before and after the conference.',
  ),
  'customAboutItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'title' => 'About CAA UK',
        'content' => '<p>CAA is an international organisation bringing together archaeologists, mathematicians and computer scientists. Its aims are to encourage communication between these disciplines, to provide a survey of present work in the field and to stimulate discussion and future progress.<a title="Membership" href="http://caaconference.org/membership/"><br /></a></p><p>CAA-UK exists to advance the aims and interests of CAA in the UK, and to provide a focus for those who may be unable, for whatever reason, to attend the international conference. A local chapter meeting is held annually (except in years when the international meeting is in the UK).</p><p>More about <a title="CAA UK Website" href="http://uk.caa-international.ork">CAA UK</a></p>',
      ),
      1 => 
      array (
        'title' => '',
        'content' => '',
      ),
    ),
  ),
  'homeHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'caa-uk-web.png',
      'uploadName' => 'homeHeaderTitleImage_en_US.png',
      'width' => 1003,
      'height' => 147,
      'dateUploaded' => '2016-12-07 04:25:20',
    ),
  ),
  'homeHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This conference utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the conference for purposes of preservation and restoration. <a href="http://lockss.stanford.edu/">More...</a>',
  ),
  'navItems' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'name' => 'CAA UK 2017',
        'isLiteral' => '1',
        'url' => 'index.php?conference=caauk&schedConf=caauk2017&page=index',
      ),
      1 => 
      array (
        'name' => 'Registration',
        'isLiteral' => '1',
        'url' => 'index.php?conference=caauk&schedConf=caauk2017&page=schedConf&op=registration',
      ),
      2 => 
      array (
        'name' => 'CAA UK',
        'isLiteral' => '1',
        'url' => 'http://uk.caa-international.org/',
        'isAbsolute' => '1',
      ),
    ),
  ),
  'pageHeaderTitleImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'caa-uk-web.png',
      'uploadName' => 'pageHeaderTitleImage_en_US.png',
      'width' => 1003,
      'height' => 147,
      'dateUploaded' => '2016-12-07 04:06:17',
    ),
  ),
  'pageHeaderTitleType' => 
  array (
    'en_US' => '1',
  ),
  'privacyStatement' => 
  array (
    'en_US' => 'The names and email addresses entered in this conference site will be used exclusively for the stated purposes of this conference and will not be made available for any other purpose or to any other party.',
  ),
  'searchDescription' => 
  array (
    'en_US' => 'CAA UK Conference Registration',
  ),
  'searchKeywords' => 
  array (
    'en_US' => 'caa, caa uk, caa uk conference',
  ),
  'title' => 
  array (
    'en_US' => 'CAA UK',
  ),
); ?>