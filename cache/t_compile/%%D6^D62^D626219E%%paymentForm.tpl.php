<?php /* Smarty version 2.6.26, created on 2017-11-20 05:12:34
         compiled from file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'translate', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 39, false),array('function', 'url', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 98, false),array('modifier', 'string_format', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 40, false),array('modifier', 'escape', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 40, false),array('modifier', 'nl2br', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 92, false),array('modifier', 'to_array', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/paymentForm.tpl', 98, false),)), $this); ?>
<?php echo ''; ?><?php $this->assign('pageTitle', "plugins.paymethod.stripemanual"); ?><?php echo ''; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo ''; ?>


<?php echo '
<script type="text/javascript">
  function selectmethod(obj) {
	if (obj.value=="stripe") {
		jQuery("#stripemanual_stripe").show();
		jQuery("#stripemanual_manual").hide();
	} else {
		jQuery("#stripemanual_stripe").hide();
		jQuery("#stripemanual_manual").show();
	}
  }
</script>
'; ?>


<table>
	<tr>
		<td>&nbsp;</td>
		<td><?php echo $this->_tpl_vars['stripemanualDescription']; ?>
</td>
	</tr>
</table>

	<?php if ($this->_tpl_vars['params']['amount']): ?>
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.purchase.amount"), $this);?>
</td>
			<td class="value" width="80%"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['params']['amount'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
<?php if ($this->_tpl_vars['params']['currency_code']): ?> (<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['currency_code'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
)<?php endif; ?></strong></td>			
		</tr>
	</table>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['params']['item_name']): ?>
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.purchase.description"), $this);?>
</td>
			<td class="value" width="80%"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['params']['item_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
</strong></td>
		</tr>
	</table>
	<?php endif; ?>


<h4><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.method"), $this);?>
</h4>

<p><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.warning"), $this);?>
</p>

<p><input type="radio" name="stripemanual_type" value="stripe" onclick="selectmethod(this);"> <?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.method.stripe"), $this);?>
</p>
<div id="stripemanual_stripe" style="display: none;">
<form action="<?php echo $this->_tpl_vars['params']['notify_url']; ?>
" id="stripemanualPaymentForm" name="stripemanualPaymentForm" method="post" style="margin-bottom: 0px;">
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/formErrors.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php $_from = $this->_tpl_vars['params']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['value']):
?>
		<input type="hidden" name="<?php echo ((is_array($_tmp=$this->_tpl_vars['name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['value'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" />
	<?php endforeach; endif; unset($_from); ?>

	<p>  <script src="<?php echo $this->_tpl_vars['stripemanualFormUrl']; ?>
" class="stripe-button"
          data-key="<?php echo $this->_tpl_vars['stripemanualPublicKey']; ?>
"
          data-amount="<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['amount'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
00" 
		  data-name="<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['confname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
"
		  data-description="<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['item_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
"
		  data-currency="<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['currency_code'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
"></script>
	</p>
</form>
</div>

<p><input type="radio" name="stripemanual_type" value="manual" onclick="selectmethod(this);"> <?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.method.manual"), $this);?>
</p>
<div id="stripemanual_manual" style="display: none;">
<input type="hidden" name="custom" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['custom'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
">
<table class="data" width="100%">
	<tr>
		<td class="label" width="20%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.purchase.title"), $this);?>
</td>
		<td class="value" width="80%"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['params']['confname'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
</strong></td>
	</tr>
	<?php if ($this->_tpl_vars['params']['amount']): ?>
		<tr>
			<td class="label" width="20%"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.purchase.fee"), $this);?>
</td>
			<td class="value" width="80%"><strong><?php echo ((is_array($_tmp=$this->_tpl_vars['params']['amount'])) ? $this->_run_mod_handler('string_format', true, $_tmp, "%.2f") : smarty_modifier_string_format($_tmp, "%.2f")); ?>
<?php if ($this->_tpl_vars['params']['currency_code']): ?> (<?php echo ((is_array($_tmp=$this->_tpl_vars['params']['currency_code'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
)<?php endif; ?></strong></td>
		</tr>
	<?php endif; ?>
	<?php if ($this->_tpl_vars['itemDescription']): ?>
	<tr>
		<td colspan="2"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['params']['item_name'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)))) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</td>
	</tr>
	<?php endif; ?>
</table>
<p><?php echo ((is_array($_tmp=$this->_tpl_vars['stripemanualInstructions'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</p>

<p><a href="<?php echo $this->_plugins['function']['url'][0][0]->smartyUrl(array('page' => 'payment','op' => 'plugin','path' => ((is_array($_tmp=((is_array($_tmp='Stripemanual')) ? $this->_run_mod_handler('to_array', true, $_tmp, 'notify', $this->_tpl_vars['params']['custom']) : $this->_plugins['modifier']['to_array'][0][0]->smartyToArray($_tmp, 'notify', $this->_tpl_vars['params']['custom'])))) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp))), $this);?>
" class="action"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.sendNotificationOfPayment"), $this);?>
</a>
</div>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "common/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>