<?php /* Smarty version 2.6.26, created on 2017-11-20 05:10:52
         compiled from file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/settingsForm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'translate', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/settingsForm.tpl', 11, false),array('function', 'fieldLabel', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/settingsForm.tpl', 14, false),array('modifier', 'escape', 'file:/homepages/5/d386757553/htdocs/caaconference.org/ocs/plugins/paymethod/stripemanual/templates/settingsForm.tpl', 16, false),)), $this); ?>
	<tr>
		<td colspan="2"><h4><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings"), $this);?>
</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'stripemanualurl','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.stripemanualurl"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="stripemanualurl" id="stripemanualurl" size="50" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['stripemanualurl'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.stripemanualurl.description"), $this);?>
<br/>
			&nbsp;
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'secret_key','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.secret_key"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="secret_key" id="secret_key" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['secret_key'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.secret_key.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'public_key','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.public_key"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="public_key" id="public_key" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['public_key'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.public_key.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'wp_site','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.wp_site"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="wp_site" id="wp_site" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['wp_site'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.wp_site.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'reg_keyword','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.reg_keyword"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="reg_keyword" id="reg_keyword" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['reg_keyword'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.reg_keyword.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'discount_keyword','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.discount_keyword"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="discount_keyword" id="discount_keyword" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['discount_keyword'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.discount_keyword.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'skip_keywords','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.skip_keywords"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="skip_keywords" id="skip_keywords" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['skip_keywords'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.skip_keywords.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'mem_list_full','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.mem_list_full"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="8" name="mem_list_full" id="mem_list_full" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['mem_list_full'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.mem_list_full.description"), $this);?>

		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'mem_list_discounted','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.mem_list_discounted"), $this);?>
</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="8" name="mem_list_discounted" id="mem_list_discounted" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['mem_list_discounted'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
" /><br/>
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.mem_list_discounted.description"), $this);?>

		</td>
	</tr>
	<?php if (! $this->_tpl_vars['isCurlInstalled']): ?>
		<tr>
			<td colspan="2">
				<span class="instruct"><?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.curlNotInstalled"), $this);?>
</span>
			</td>
		</tr>
	<?php endif; ?>

	<tr valign="top">
		<td class="label" width="20%"><?php echo $this->_plugins['function']['fieldLabel'][0][0]->smartyFieldLabel(array('name' => 'stripemanualInstructions','required' => 'true','key' => "plugins.paymethod.stripemanual.settings.instructions"), $this);?>
</td>
		<td class="value" width="80%">
			<?php echo $this->_plugins['function']['translate'][0][0]->smartyTranslate(array('key' => "plugins.paymethod.stripemanual.settings.stripemanualInstructions"), $this);?>
<br />
			<textarea name="stripemanualInstructions" id="stripemanualInstructions" rows="12" cols="60" class="textArea"><?php echo ((is_array($_tmp=$this->_tpl_vars['stripemanualInstructions'])) ? $this->_run_mod_handler('escape', true, $_tmp) : $this->_plugins['modifier']['escape'][0][0]->smartyEscape($_tmp)); ?>
</textarea>
		</td>
	</tr>