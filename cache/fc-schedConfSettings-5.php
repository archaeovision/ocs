<?php return array (
  'acceptSupplementaryReviewMaterials' => false,
  'closeCommentsDate' => 1437519599,
  'contactEmail' => 'caa2016conference@khm.uio.no',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'contactName' => 'Espen Uleberg',
  'contactPhone' => '',
  'contactTitle' => '',
  'contributors' => 
  array (
  ),
  'copySubmissionAckAddress' => '',
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckSpecified' => false,
  'delayOpenAccessDate' => 1437433200,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'endDate' => 1459551600,
  'envelopeSender' => '',
  'locationAddress' => '',
  'locationCity' => 'Oslo',
  'locationCountry' => 'NO',
  'locationName' => 'Museum of Cultural History, University of Oslo',
  'metaCitations' => false,
  'metaCoverage' => false,
  'metaDiscipline' => false,
  'metaSubject' => false,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaType' => false,
  'notifyAllAuthorsOnDecision' => true,
  'numDaysBeforeInviteReminder' => 3,
  'numDaysBeforeSubmitReminder' => 0,
  'numWeeksPerReview' => 4,
  'numWeeksPerReviewAbsolute' => 1447545600,
  'numWeeksPerReviewRelative' => 0,
  'paymentMethodPluginName' => 'ManualPayment',
  'postAbstractsDate' => 1437433200,
  'postAccommodation' => false,
  'postCFP' => true,
  'postOverview' => false,
  'postPapersDate' => 1437433200,
  'postPayment' => false,
  'postPresentations' => false,
  'postProgram' => false,
  'postProposalSubmission' => false,
  'postScheduleDate' => 1437433200,
  'postSupporters' => false,
  'postTimeline' => false,
  'postTrackPolicies' => false,
  'previewAbstracts' => false,
  'rateReviewerOnQuality' => 0,
  'regAuthorCloseDate' => 1445990399,
  'regAuthorOpenDate' => 1437433200,
  'regReviewerCloseDate' => 1469141999,
  'regReviewerOpenDate' => 1437433200,
  'remindForInvite' => 1,
  'remindForSubmit' => 1,
  'restrictReviewerFileAccess' => 1,
  'reviewDeadlineType' => 2,
  'reviewerAccessKeysEnabled' => 0,
  'reviewMode' => 0,
  'showCFPDate' => 1442790000,
  'sponsors' => 
  array (
  ),
  'startDate' => 1459206000,
  'statisticsTrackIds' => 
  array (
    0 => '165',
  ),
  'submissionsCloseDate' => 1445990399,
  'submissionsOpenDate' => 1442790000,
  'supportEmail' => 'caa2016conference@khm.uio.no',
  'supportName' => 'Espen Uleberg',
  'supportPhone' => '',
  'acronym' => 
  array (
    'en_US' => 'CAA2016',
  ),
  'cfpMessage' => 
  array (
    'en_US' => 'Oral papers will present new and ground breaking research within the session themes. Oral presentations are limited to 20 minutes, with an additional 5 minutes provided at the end of each paper for questions. Abstracts can contain up to 300 words.

Presentations must be prepared in a way that is self-contained and work on all operating systems.

One person may present one oral paper, but may also be a co-author of multiple papers. Authors can present both an oral paper and a poster.

We will organise a digital poster gallery, in addition to the traditional printed poster presentations.',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'Museum of Cultural History, University of Oslo',
  ),
  'emailSignature' => 
  array (
    'en_US' => '_______________________________________________
CAA 2016
http://caaconference.org/
Twitter: @CAAOslo',
  ),
  'introduction' => 
  array (
    'en_US' => '<img style="width: 100%;" src="http://sites.caa-international.org/caa2016/wp-content/uploads/sites/15/2015/07/cropped-motto3.jpg" width="100%" alt="" />

The 44th Computer Applications and Quantitative Methods in Archaeology Conference (CAA 2016) has been given the theme "Exploring Oceans of Data", in reference to Norway\'s maritime heritage. The conference will address a multitude of topics. Through diverse case studies from all over the world, the conference will showcase ground-breaking technologies and best practice from various archaeological and computer-science disciplines. Archaeological documentation is, to an ever greater extent, born digital. Together with decades of digitalisation of archive material, we now have deep oceans of digital information that can and should be explored. Rivers worth of large data sets come from 3D, GIS, LIDAR, as well as near surface geophysical prospection. 

The conference will bring together hundreds of participants from around the globe, immersing delegates in parallel sessions, workshops, tutorials and roundtables. The conference will be held at the University of Oslo, Norway, from March 29th to April 2nd 2016. 

We warmly welcome participants and contributors from every corner of the world to the beautiful capital city of Norway. The harbour city of Oslo houses the spectacular Viking ships, which bravely set sail into the deep oceans some 1200 years ago. 

Conference website: <a href="http://2016.caaconference.org">caaconference.org</a>

For general information about the conference: <a href="mailto:caa2016conference@khm.uio.no">caa2016conference@khm.uio.no</a>',
  ),
  'title' => 
  array (
    'en_US' => 'CAA 2016',
  ),
); ?>