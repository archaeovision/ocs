<?php return array (
  'numWeeksPerReview' => 4,
  'reviewDeadlineType' => 0,
  'emailSignature' => 
  array (
    'en_US' => '________________________________________________________________________
CAA UK 
http://uk.caa-international.org',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'order' => '1',
        'content' => 'The submission has not been previously published, nor is it before another conference for consideration (or an explanation has been provided in Comments to the Director).',
      ),
      1 => 
      array (
        'order' => '2',
        'content' => 'The submission file is in OpenOffice, Microsoft Word, RTF, or WordPerfect document file format.',
      ),
      2 => 
      array (
        'order' => '3',
        'content' => 'All URL addresses in the text (e.g., <a href="http://pkp.sfu.ca">http://pkp.sfu.ca</a>) are activated and ready to click.',
      ),
      3 => 
      array (
        'order' => '4',
        'content' => 'The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.',
      ),
      4 => 
      array (
        'order' => '5',
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the <a href="/index.php?conference=caauk&amp;schedConf=caauk2017&amp;page=about&amp;op=submissions#authorGuidelines">Author Guidelines</a>, which is found in About the Conference.',
      ),
      5 => 
      array (
        'order' => '6',
        'content' => 'If submitting to a peer-reviewed track of the conference, authors\' names are removed from submission, with "Author" and year used in the bibliography and footnotes, instead of authors\' name, paper title, etc.',
      ),
      6 => 
      array (
        'order' => '7',
        'content' => 'If submitting to peer review, all Microsoft Office documents (including Supplementary Files) have been saved by going to File and selecting Save As; clicking Tools (or Options in a Mac); clicking Security; selecting "Remove personal information from file properties on save"; clicking Save.',
      ),
    ),
  ),
  'paymentMethodPluginName' => 'Stripe',
  'title' => 
  array (
    'en_US' => 'CAA UK 2017',
  ),
  'acronym' => 
  array (
    'en_US' => 'CAA UK 2017',
  ),
  'postTimeline' => false,
  'postOverview' => false,
  'postCFP' => false,
  'postProposalSubmission' => false,
  'postTrackPolicies' => false,
  'postProgram' => false,
  'postPresentations' => false,
  'postAccommodation' => false,
  'postSupporters' => true,
  'postPayment' => true,
  'startDate' => 1488585600,
  'endDate' => 1488672000,
  'regAuthorOpenDate' => 1480896000,
  'regAuthorCloseDate' => 1480982399,
  'showCFPDate' => 1480896000,
  'submissionsOpenDate' => 1480982400,
  'submissionsCloseDate' => 1481068799,
  'regReviewerOpenDate' => 1480982400,
  'regReviewerCloseDate' => 1481068799,
  'postAbstractsDate' => 1480982400,
  'postScheduleDate' => 1480982400,
  'postSchedule' => false,
  'postPapersDate' => 1480982400,
  'delayOpenAccessDate' => 1480982400,
  'closeCommentsDate' => 1481068799,
  'introduction' => 
  array (
    'en_US' => 'The next annual meeting of the UK Chapter of Computer Applications and Quantitative Methods in Archaeology (CAA-UK) will be held in Winchester on the 4th and 5th March 2017. CAA-UK aims to encourage communication between UK-based archaeologists, mathematicians and computer scientists in order to stimulate research and promote best practice in computational and mathematical approaches to the past.',
  ),
  'locationName' => 'Queen Elizabeth II Court, Winchester',
  'locationAddress' => '<p>Sussex Street</p><p><span>SO23 8UT</span></p>',
  'locationCity' => 'Winchester',
  'locationCountry' => 'GB',
  'contactName' => 'James Miles',
  'contactTitle' => '',
  'contactAffiliation' => 
  array (
    'en_US' => 'Archaeovision',
  ),
  'contactEmail' => 'james@archaeovision.eu',
  'contactPhone' => '',
  'contactFax' => '',
  'contactMailingAddress' => '',
  'supportName' => 'Hembo Pagi',
  'supportEmail' => 'hembo@archaeovision.eu',
  'supportPhone' => '',
  'envelopeSender' => '',
  'sponsors' => 
  array (
    0 => 
    array (
      'institution' => 'Archaeovision',
      'address' => '',
      'url' => 'http://archaeovision.eu',
    ),
  ),
  'contributors' => 
  array (
  ),
  'registrationName' => 'James Miles',
  'registrationEmail' => 'james@archaeovision.eu',
  'registrationPhone' => '',
  'registrationFax' => '',
  'registrationMailingAddress' => '',
  'enableOpenAccessNotification' => false,
  'enableAuthorSelfArchive' => false,
  'enableRegistrationExpiryReminderBeforeMonths' => false,
  'numMonthsBeforeRegistrationExpiryReminder' => 0,
  'enableRegistrationExpiryReminderBeforeWeeks' => false,
  'numWeeksBeforeRegistrationExpiryReminder' => 0,
  'enableRegistrationExpiryReminderAfterMonths' => false,
  'numMonthsAfterRegistrationExpiryReminder' => 0,
  'enableRegistrationExpiryReminderAfterWeeks' => false,
  'numWeeksAfterRegistrationExpiryReminder' => 0,
  'sponsorNote' => 
  array (
    'en_US' => '<p>CAA UK 2017 is hosted by <a title="Archaeovision Website" href="http://archaeovision.eu">Archaeovision</a></p><p> </p>',
  ),
  'reviewMode' => 0,
  'previewAbstracts' => false,
  'acceptSupplementaryReviewMaterials' => false,
  'copySubmissionAckPrimaryContact' => false,
  'copySubmissionAckSpecified' => false,
  'copySubmissionAckAddress' => '',
  'metaDiscipline' => false,
  'metaSubjectClass' => false,
  'metaSubjectClassUrl' => 'http://',
  'metaSubject' => false,
  'metaCoverage' => false,
  'metaType' => false,
  'metaCitations' => false,
  'enablePublicPaperId' => false,
  'enablePublicSuppFileId' => false,
  'overview' => 
  array (
    'en_US' => '<p>Computational archaeology is an ever-changing discipline, with newer software and processing packages regularly becoming available. The introduction of new methods through novel software and recording solutions provides a basis in which archaeology can adapt, and at CAA-UK 2017 we have invited papers that introduce these new methods to the discipline, assessing how they can add to our understanding of the past. Papers that focus on evaluating current computational practises, reviewing how the discipline can change, and how the discipline can adapt to achieve more robust scientific outcomes, have also been encouraged. As with the introduction of newer methods, we papers that follow the suggested topics below will also be presented:</p><ul><li>Integration of scientific and theoretical methods in computing</li><li>Visualisation &amp; Mixed Reality in Archaeology</li><li>GIS &amp; Geospatial Analysis</li><li>Geophysics &amp; Remote sensing</li><li>Photogrammetry &amp; 3D Recording</li><li>Statistical methods</li><li>Semantic web</li><li>Public Engagement</li><li>Visualisation &amp; 3D modelling</li><li>Social media</li><li>Data management</li></ul>',
  ),
  'remindForInvite' => 0,
  'remindForSubmit' => 0,
  'rateReviewerOnQuality' => 0,
  'restrictReviewerFileAccess' => 0,
  'reviewerAccessKeysEnabled' => 0,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'numWeeksPerReviewRelative' => 0,
  'numWeeksPerReviewAbsolute' => 1482105600,
  'notifyAllAuthorsOnDecision' => false,
  'boardEnabled' => false,
); ?>