{**
 * navbar.tpl
 *
 * Copyright (c) 2000-2011 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Navigation Bar
 *
 *}
<div id="navbar">
	<ul class="menu">
		<li><a href="{url context=$homeContext op="index"}">{translate key="navigation.home"}</a></li>
{*		<li><a href="{url page="about"}">{translate key="navigation.about"}</a></li> *}
		{if $isUserLoggedIn}
			<li><a href="{url conference="index" page="user"}">{translate key="navigation.userHome"}</a></li>
		{else}
			<li><a href="{url page="login"}">{translate key="navigation.login"}</a></li>
			<li><a href="{url page="user" op="account"}">{translate key="navigation.account"}</a></li>
		{/if}{* $isUserLoggedIn *}

{*		<li><a href="{url page="search"}">{translate key="navigation.search"}</a></li> *}

		{foreach from=$navMenuItems item=navItem}
			{if $navItem.url != '' && $navItem.name != ''}
				<li><a href="{if $navItem.isAbsolute}{$navItem.url|escape}{else}{$navItem.url|escape}{/if}">{if $navItem.isLiteral}{$navItem.name|escape}{else}{translate key=$navItem.name}{/if}</a></li>
			{/if}
		{/foreach}
	</ul>
</div>
