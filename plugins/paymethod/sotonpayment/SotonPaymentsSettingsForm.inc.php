<?php

/**
 * @file SotonPaymentsSettingsForm.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SotonPaymentsSettingsForm
 * @ingroup plugins_paymethod_sotonpayments
 * @see SotonPaymentsPlugin
 *
 * @brief Form for conference managers to edit the SotonPayments Settings
 * 
 */
 
import('form.Form');

class SotonPaymentsSettingsForm extends Form {
	/** @var $schedConfId int */
	var $schedConfId;

	/** @var $plugin object */
	var $plugin;

	/** $var $errors string */
	var $errors;

	/**
	 * Constructor
	 * @param $schedConfId int
	 */
	function SotonPaymentsSettingsForm(&$plugin, $conferenceId, $schedConfId) {
		parent::Form($plugin->getTemplatePath() . 'settingsForm.tpl');

		$this->addCheck(new FormValidatorPost($this));

		$this->conferenceId = $conferenceId;
		$this->schedConfId = $schedConfId;
		$this->plugin =& $plugin;

	}



	/**
	 * Initialize form data from current group group.
	 */
	function initData( ) {
		$schedConfId = $this->schedConfId;
		$conferenceId = $this->conferenceId;
		$plugin =& $this->plugin;

		$this->_data = array(
			'enabled' => $plugin->getSetting($conferenceId, $schedConfId, 'enabled'),
			'sotonpaymentsurl' => $plugin->getSetting($conferenceId, $schedConfId, 'sotonpaymentsurl'),
			'g_returnWords' => $plugin->getSetting($conferenceId, $schedConfId, 'g_returnWords'),
			'g_emailfooter' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailfooter'),
			'g_emailsubject' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailsubject'),
			'g_emailFrom' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailFrom'),
			'g_Category' => $plugin->getSetting($conferenceId, $schedConfId, 'g_Category'),
			'g_School' => $plugin->getSetting($conferenceId, $schedConfId, 'g_School'),
			'g_CostCentre' => $plugin->getSetting($conferenceId, $schedConfId, 'g_CostCentre'),
			'g_Name' => $plugin->getSetting($conferenceId, $schedConfId, 'g_Name')
		);

	}

	/**
	 * Assign form data to user-submitted data.
	 */
	function readInputData() {
		$this->readUserVars(array('enabled',
								'sotonpaymentsurl', 
								'g_returnWords', 
								'g_emailfooter', 
								'g_emailsubject', 
								'g_emailFrom', 
								'g_Category', 
								'g_School', 
								'g_CostCentre',
								'g_Name'
								));
	}

	/**
	 * Save page - write to content file. 
	 */	 
	function save() {
		$plugin =& $this->plugin;
		$conferenceId = $this->conferenceId;
		$schedConfId = $this->schedConfId;
		$sotonpaymentsSettings = array();
		$plugin->updateSetting($conferenceId, $schedConfId, 'enabled', $this->getData('enabled'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'sotonpaymentsurl', $this->getData('sotonpaymentsurl'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_returnWords', $this->getData('g_returnWords'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailfooter', $this->getData('g_emailfooter'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailsubject', $this->getData('g_emailsubject'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailFrom', $this->getData('g_emailFrom'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_Category', $this->getData('g_Category'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_School', $this->getData('g_School'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_CostCentre', $this->getData('g_CostCentre'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_Name', $this->getData('g_Name'));
		
	}
}

?>
