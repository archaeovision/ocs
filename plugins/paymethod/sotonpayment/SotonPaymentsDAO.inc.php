<?php

/**
 * @file SotonPaymentsDAO.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SotonPaymentsDAO
 * @ingroup plugins_paymethod_sotonpayments
 * @see SotonPaymentsPlugin
 *
 * @brief Class for SotonPayments Logging DAO.
 * Operations for retrieving and modifying Transactions objects.
 */

import('db.DAO');

class SotonPaymentsDAO extends DAO {

	/**
	 * Constructor.
	 */
	function SotonPaymentsDAO() {
		parent::DAO();
	}

	/*
	 * Insert a payment into the payments table
	 */
	function insertTransaction($g_ref, $g_key, $refNum, $payid) {
		$ret = $this->update(
			'INSERT INTO onestoppayments_transactions (
				g_ref,
				g_key,
				refNum,
				payid)
				VALUES 
				(?, ?, ?, ?)'
			, 
			array(
				$g_ref,
				$g_key,
				$refNum,
				$payid
			) 
		);

		return true;
	}

	function transactionExists($g_ref) {
		$result =& $this->retrieve(
			'SELECT count(*) 
				FROM onestoppayments_transactions
				WHERE g_ref = ?', 
				array($g_ref)
		);

		$returner = false;
		if (isset($result->fields[0]) && $result->fields[0] >= 1) 
			$returner = true;

		$result->Close();
		return $returner;		
	}
}

?>
