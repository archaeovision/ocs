<?php

/**
 * @file SotonPaymentsPlugin.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SotonPaymentsPlugin
 * @ingroup plugins_paymethod_sotonpayments
 * @see SotonPaymentsDAO, SotonPaymentsPaymentForm, SotonPaymentsSettingsForm
 *
 * @brief SotonPayments plugin class
 *
 */

import('classes.plugins.PaymethodPlugin');

class SotonPaymentsPlugin extends PaymethodPlugin {

	function getName() {
		return 'SotonPayments';
	}

	function getDisplayName() {
		return Locale::translate('plugins.paymethod.sotonpayments.displayName');
	}

	function getDescription() {
		return Locale::translate('plugins.paymethod.sotonpayments.description');
	}   

	function register($category, $path) {
		if (parent::register($category, $path)) {			
			$this->addLocaleData();
			$this->import('SotonPaymentsDAO');
			$SotonPaymentsDAO =& new SotonPaymentsDAO();
			DAORegistry::registerDAO('SotonPaymentsDAO', $SotonPaymentsDAO);
			return true;
		}
		return false;
	}

	function getSettingsFormFieldNames() {
		return array('sotonpaymentsurl','g_School','g_Category','g_CostCentre','g_emailFrom','g_emailsubject','g_emailfooter','g_returnWords','g_Name');
	}

	function isCurlInstalled() {
		return (function_exists('curl_init'));
	}

	function isConfigured() {
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return false;

		// Make sure CURL support is included.
		if (!$this->isCurlInstalled()) return false;

		// Make sure that all settings form fields have been filled in
		foreach ($this->getSettingsFormFieldNames() as $settingName) {
			$setting = $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), $settingName);
			if (empty($setting)) return false;
		}
		return true;
	}

	function displayPaymentSettingsForm(&$params, &$smarty) {
		$smarty->assign('isCurlInstalled', $this->isCurlInstalled());
		return parent::displayPaymentSettingsForm($params, $smarty);
	}

	function displayPaymentForm($queuedPaymentId, &$queuedPayment) {
		if (!$this->isConfigured()) return false;
		$schedConf =& Request::getSchedConf();
		$user =& Request::getUser();
		
		$params = array(
		    'TRAN-TYPE' => '810',
		    'OSCREGO' => 'R43762',
			'FNAME'  => ($user)?$user->getFirstName():'',
			'SNAME'  => ($user)?$user->getLastname():'',
			'UnitAmountIncTax'  => $queuedPayment->getAmount(),
			'g_PostURL'  => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'ipn')),
			'g_ref'  => $queuedPaymentId,
			/*
			'g_Name'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_courseCode'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_startUrl'  => Request::url(null, null, 'payment', 'plugin', array($this->getName())),
			'g_returnURL'  => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'return')),
			'g_totalamount'  => $queuedPayment->getAmount(),
			'g_School'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_School'),
			'g_FirstName'  => ($user)?$user->getFirstName():'',
			'g_LastName'  => ($user)?$user->getLastname():'',
			'g_Category'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Category'),
			'g_CostCentre'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_CostCentre'),
			'g_emailFrom'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailFrom'),
			'g_emailbcc'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailFrom'),
			'g_emailsubject'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailsubject'),
			'g_emailfooter'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailfooter'),
			'g_returnWords'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_returnWords'),
			'g_PostURL'  => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'ipn')),
			'g_ref'  => $queuedPaymentId,
			'g_key'  => '',
			'g_itemdesc1'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_itemamount1'  => $queuedPayment->getAmount()
			*/
		);

		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign('params', $params);
		$templateMgr->assign('SotonPaymentsUrl', $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'sotonpaymentsurl'));
		$templateMgr->display($this->getTemplatePath() . 'paymentForm.tpl');
	}

	/**
	 * Handle incoming requests/notifications
	 */
	function handle($args) {
		$templateMgr =& TemplateManager::getManager();
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return parent::handle($args);

		// Just in case we need to contact someone
		import('mail.MailTemplate');
		$contactName = $schedConf->getSetting('contactName');
		$contactEmail = $schedConf->getSetting('contactEmail');
		$mail = &new MailTemplate('SOTONPAYMENTS_INVESTIGATE');
		$mail->setFrom($contactEmail, $contactName);
		$mail->addRecipient($contactEmail, $contactName);

		$paymentStatus = Request::getUserVar('payment_status');
		switch (array_shift($args)) {
			case 'ipn':
				$debugmail = &new MailTemplate('SOTONPAYMENTS_INVESTIGATE');
				$debugmail->setFrom($contactEmail, $contactName);
				$debugmail->addRecipient('tepb@soton.ac.uk', $contactName);
				$debugmail->assignParams(array(
					'schedConfName' => $schedConf->getFullTitle(),
					'postInfo' => print_r($_POST, true),
					'additionalInfo' => "Debug Email",
					'serverVars' => print_r($_SERVER, true)
				));
				$debugmail->send();

				$SotonPaymentsDAO =& DAORegistry::getDAO('SotonPaymentsDAO');
				$g_ref=Request::getUserVar('g_ref');
				if ($SotonPaymentsDAO->transactionExists($g_ref)) {
					// A duplicate transaction was received; notify someone.
					$mail->assignParams(array(
						'schedConfName' => $schedConf->getFullTitle(),
						'postInfo' => print_r($_POST, true),
						'additionalInfo' => "Duplicate transaction ID: $g_ref",
						'serverVars' => print_r($_SERVER, true)
					));
					$mail->send();
					exit();
				} else {
					// New transaction succeeded. Record it.
					$SotonPaymentsDAO->insertTransaction(
						$g_ref,
						Request::getUserVar('g_key'),
						Request::getUserVar('refNum'),
						Request::getUserVar('payid')
					);
					import('payment.ocs.OCSPaymentManager');
					$ocsPaymentManager =& OCSPaymentManager::getManager();
					$queuedPayment =& $ocsPaymentManager->getQueuedPayment($g_ref);
					echo 'rix: step1';
					if (!$queuedPayment) {
					echo 'rix: no queuedPayment';
						// The queued payment entry is missing. Complain.
						$mail->assignParams(array(
							'schedConfName' => $schedConf->getFullTitle(),
							'postInfo' => print_r($_POST, true),
							'additionalInfo' => "Missing queued payment ID: $g_ref",
							'serverVars' => print_r($_SERVER, true)
						));
						$mail->send();
						exit();
					}
					// Fulfill the queued payment.
					echo 'rix: step2';
					if ($ocsPaymentManager->fulfillQueuedPayment($g_ref, $queuedPayment)) {
					echo 'rix: step3';
						// Send the registrant a notification that their payment was received
						$schedConfSettingsDao =& DAORegistry::getDAO('SchedConfSettingsDAO');

						$schedConfId = $schedConf->getId();
						$registrationName = $schedConfSettingsDao->getSetting($schedConfId, 'registrationName');
						$registrationEmail = $schedConfSettingsDao->getSetting($schedConfId, 'registrationEmail');
						$registrationPhone = $schedConfSettingsDao->getSetting($schedConfId, 'registrationPhone');
						$registrationFax = $schedConfSettingsDao->getSetting($schedConfId, 'registrationFax');
						$registrationMailingAddress = $schedConfSettingsDao->getSetting($schedConfId, 'registrationMailingAddress');
						$registrationContactSignature = $registrationName;

						if ($registrationMailingAddress != '') $registrationContactSignature .= "\n" . $registrationMailingAddress;
						if ($registrationPhone != '') $registrationContactSignature .= "\n" . Locale::Translate('user.phone') . ': ' . $registrationPhone;
						if ($registrationFax != '')	$registrationContactSignature .= "\n" . Locale::Translate('user.fax') . ': ' . $registrationFax;

						$registrationContactSignature .= "\n" . Locale::Translate('user.email') . ': ' . $registrationEmail;

						$paramArray = array(
							'registrantName' => $schedConf->getSetting('contactName'),
							'schedConfName' => $schedConf->getFullTitle(),
							'registrationContactSignature' => $registrationContactSignature 
						);

						$userDao =& DAORegistry::getDAO('UserDAO');
						$user = &$userDao->getUser($queuedPayment->getUserId());
						$useremail=$user->getEmail();

						$userFullName =$user->getFullName();

						import('mail.MailTemplate');
						$mail = new MailTemplate('MANUAL_PAYMENT_RECEIVED');
						$mail->setFrom($registrationEmail, $registrationName);
						$mail->assignParams($paramArray);
						$mail->addRecipient($useremail, $userFullName);
						$mail->send();
						exit();
					}
					// If we're still here, it means the payment couldn't be fulfilled.
				echo 'no fulfill';
					$mail->assignParams(array(
						'schedConfName' => $schedConf->getFullTitle(),
						'postInfo' => print_r($_POST, true),
						'additionalInfo' => "Queued payment ID $queuedPaymentId could not be fulfilled.",
						'serverVars' => print_r($_SERVER, true)
					));
					$mail->send();
				}
				echo 'step 4';
				exit();
			case 'return':
				Request::redirect(null, null, 'index');
				break;
		}
		parent::handle($args); // Don't know what to do with it
	}

	function getInstallSchemaFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'schema.xml');
	}

	function getInstallDataFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'data.xml');
	}
}

?>
