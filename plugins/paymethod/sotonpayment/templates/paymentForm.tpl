{**
 * paymentForm.tpl
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for submitting a SotonPayments payment
 *
 *}
{assign var="pageTitle" value="plugins.paymethod.sotonpayments"}
{include file="common/header.tpl"}

<table>
	<tr>
		<td>{$sotonpaymentsDescription}</td>
	</tr>
</table>

<p>{translate key="plugins.paymethod.sotonpayments.warning"}</p>

<form action="{$SotonPaymentsUrl}" id="sotonpaymentsPaymentForm" name="sotonpaymentsPaymentForm" method="post" style="margin-bottom: 0px;">
	{include file="common/formErrors.tpl"}
	{if $params.amount}
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%">{translate key="plugins.paymethod.sotonpayments.purchase.amount"}</td>
			<td class="value" width="80%"><strong>{$params.amount|string_format:"%.2f"}{if $params.currency_code} ({$params.currency_code|escape}){/if}</strong></td>
		</tr>
	</table>
	{/if}
	{foreach from=$params key="name" item="value"}
		<input type="hidden" name="{$name|escape}" value="{$value|escape}" />
	{/foreach}

	<p><input type="submit" name="submitBtn" value="{translate key="common.continue"}" class="button defaultButton" /></p>
</form>

{include file="common/footer.tpl"}
