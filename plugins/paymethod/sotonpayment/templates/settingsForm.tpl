{**
 * settingsForm.tpl
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for SotonPayments settings.
 *
 *}
	<tr>
		<td colspan="2"><h4>{translate key="plugins.paymethod.sotonpayments.settings"}</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="sotonpaymentsurl" required="true" key="plugins.paymethod.sotonpayments.settings.sotonpaymentsurl"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="sotonpaymentsurl" id="sotonpaymentsurl" size="50" value="{$sotonpaymentsurl|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.sotonpaymentsurl.description"}<br/>
			&nbsp;
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_School" required="true" key="plugins.paymethod.sotonpayments.settings.g_School"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_School" id="g_School" value="{$g_School|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_School.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_Category" required="true" key="plugins.paymethod.sotonpayments.settings.g_Category"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_Category" id="g_Category" value="{$g_Category|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_Category.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_CostCentre" required="true" key="plugins.paymethod.sotonpayments.settings.g_CostCentre"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_CostCentre" id="g_CostCentre" value="{$g_CostCentre|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_CostCentre.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailFrom" required="true" key="plugins.paymethod.sotonpayments.settings.g_emailFrom"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailFrom" id="g_emailFrom" value="{$g_emailFrom|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_emailFrom.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailsubject" required="true" key="plugins.paymethod.sotonpayments.settings.g_emailsubject"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailsubject" id="g_emailsubject" value="{$g_emailsubject|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_emailsubject.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailfooter" required="true" key="plugins.paymethod.sotonpayments.settings.g_emailfooter"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailfooter" id="g_emailfooter" value="{$g_emailfooter|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_emailfooter.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_returnWords" required="true" key="plugins.paymethod.sotonpayments.settings.g_returnWords"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_returnWords" id="g_returnWords" value="{$g_returnWords|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_returnWords.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_Name" required="true" key="plugins.paymethod.sotonpayments.settings.g_Name"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_Name" id="g_Name" value="{$g_Name|escape}" /><br/>
			{translate key="plugins.paymethod.sotonpayments.settings.g_Name.description"}
		</td>
	</tr>
	{if !$isCurlInstalled}
		<tr>
			<td colspan="2">
				<span class="instruct">{translate key="plugins.paymethod.sotonpayments.settings.curlNotInstalled"}</span>
			</td>
		</tr>
	{/if}
