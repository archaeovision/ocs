<?php

/**
 * @defgroup plugins_paymethod_sotonpayments
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for SotonPayments plugin.
 *
 * @ingroup plugins_paymethod_sotonpayments
 */

require_once('SotonPaymentsPlugin.inc.php');

return new SotonPaymentsPlugin();

?> 
