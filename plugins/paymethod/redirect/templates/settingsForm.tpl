{**
 * settingsForm.tpl
 *
 * Copyright (c) 2000-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for redirect payment settings.
 *
 *}
	<tr>
		<td colspan="2"><h4>{translate key="plugins.paymethod.redirect.settings"}</h4></td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="redirectInstructions" required="true" key="plugins.paymethod.redirect.settings.instructions"}</td>
		<td class="value" width="80%">
			{translate key="plugins.paymethod.redirect.settings.redirectInstructions"}<br />
			<textarea name="redirectInstructions" id="redirectInstructions" rows="12" cols="60" class="textArea">{$redirectInstructions|escape}</textarea>
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="redirectUrl" required="true" key="plugins.paymethod.redirect.settings.url"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="redirectUrl" id="redirectUrl" size="50" value="{$redirectUrl|escape}" /><br/>
			{translate key="plugins.paymethod.redirect.settings.urlDescription"}<br/>
			&nbsp;
		</td>
	</tr>
