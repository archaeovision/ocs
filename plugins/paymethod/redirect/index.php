<?php

/**
 * @defgroup plugins_paymethod_redirect
 */
 
/**
 * @file plugins/paymethod/redirect/index.php
 *
 * Copyright (c) 2000-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for redirect payment plugin.
 *
 * @ingroup plugins_paymethod_redirect
 */

//$Id$

require_once('RedirectPaymentPlugin.inc.php');

return new RedirectPaymentPlugin();

?> 
