<?php

/**
 * @file plugins/paymethod/redirect/RedirectPaymentPlugin.inc.php
 *
 * Copyright (c) 2000-2012 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class RedirectPaymentPlugin
 * @ingroup plugins_paymethod_redirect
 *
 * @brief Redirect payment plugin class
 *
 */

//$Id$

import('classes.plugins.PaymethodPlugin');

class RedirectPaymentPlugin extends PaymethodPlugin {

	function getName() {
		return 'RedirectPayment';
	}

	function getDisplayName() {
		return __('plugins.paymethod.redirect.displayName');
	}

	function getDescription() {
		return __('plugins.paymethod.redirect.description');
	}

	function register($category, $path) {
		if (parent::register($category, $path)) {
			$this->addLocaleData();
			return true;
		}
		return false;
	}

	function getSettingsFormFieldNames() {
		return array('redirectInstructions', 'redirectUrl');
	}

	function isConfigured() {
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return false;

		// Make sure that all settings form fields have been filled in
		foreach ($this->getSettingsFormFieldNames() as $settingName) {
			$setting = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), $settingName);
			if (empty($setting)) return false;
		}

		return true;
	}

	function displayPaymentForm($queuedPaymentId, &$queuedPayment) {
		if (!$this->isConfigured()) return false;
		$schedConf =& Request::getSchedConf();
		$templateMgr =& TemplateManager::getManager();
		$user =& Request::getUser();

		$templateMgr->assign('itemName', $queuedPayment->getName());
		$templateMgr->assign('itemDescription', $queuedPayment->getDescription());
		if ($queuedPayment->getAmount() > 0) {
			$templateMgr->assign('itemAmount', $queuedPayment->getAmount());
			$templateMgr->assign('itemCurrencyCode', $queuedPayment->getCurrencyCode());
		}
		$templateMgr->assign('redirectInstructions', $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'redirectInstructions'));
		$templateMgr->assign('redirectUrl', $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'redirectUrl'));
		$templateMgr->assign('queuedPaymentId', $queuedPaymentId);

		$templateMgr->display($this->getTemplatePath() . 'paymentForm.tpl');
	}

	/**
	 * Handle incoming requests/notifications
	 */
	function handle($args) {
		$conference =& Request::getConference();
		$schedConf =& Request::getSchedConf();
		$templateMgr =& TemplateManager::getManager();
		$user =& Request::getUser();
		$op = isset($args[0])?$args[0]:null;
		$queuedPaymentId = isset($args[1])?((int) $args[1]):0;

		import('payment.ocs.OCSPaymentManager');
		$ocsPaymentManager =& OCSPaymentManager::getManager();
		$queuedPayment =& $ocsPaymentManager->getQueuedPayment($queuedPaymentId);
		// if the queued payment doesn't exist, redirect away from payments
		if ( !$queuedPayment ) Request::redirect(null, null, null, 'index');

		switch ( $op ) {
			case 'notify':
				import('mail.MailTemplate');
				AppLocale::requireComponents(array(LOCALE_COMPONENT_APPLICATION_COMMON));
				$contactName = $schedConf->getSetting('registrationName');
				$contactEmail = $schedConf->getSetting('registrationEmail');
				$mail = new MailTemplate('REDIRECT_PAYMENT_NOTIFICATION');
				$mail->setFrom($contactEmail, $contactName);
				$mail->addRecipient($contactEmail, $contactName);
				$mail->assignParams(array(
					'schedConfName' => $schedConf->getFullTitle(),
					'userFullName' => $user?$user->getFullName():('(' . __('common.none') . ')'),
					'userName' => $user?$user->getUsername():('(' . __('common.none') . ')'),
					'itemName' => $queuedPayment->getName(),
					'itemCost' => $queuedPayment->getAmount(),
					'itemCurrencyCode' => $queuedPayment->getCurrencyCode()
				));
				$mail->send();

				$templateMgr->assign(array(
					'currentUrl' => Request::url(null, null, null, 'payment', 'plugin', array('notify', $queuedPaymentId)),
					'pageTitle' => 'plugins.paymethod.redirect.paymentNotification',
					'message' => 'plugins.paymethod.redirect.notificationSent',
					'backLink' => Request::url(null, null, 'index'),
					'backLinkLabel' => 'common.continue'
				));
				$templateMgr->display('common/message.tpl');
				exit();
				break;
		}
		parent::handle($args); // Don't know what to do with it
	}

	function getInstallEmailTemplatesFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'emailTemplates.xml');
	}

	function getInstallEmailTemplateDataFile() {
		return ($this->getPluginPath() . '/locale/{$installedLocale}/emailTemplates.xml');
	}
}

?>
