<?php

/**
 * @file OneStopPaymentsPlugin.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class OneStopPaymentsPlugin
 * @ingroup plugins_paymethod_onestoppayments
 * @see OneStopPaymentsDAO, OneStopPaymentsPaymentForm, OneStopPaymentsSettingsForm
 *
 * @brief OneStopPayments plugin class
 *
 */

import('classes.plugins.PaymethodPlugin');

// check if value exist, if not, nothing is returned
function printNotEmptyOpt($value) {
	if (empty($value))
	    return "";
	else
	    return $value;
}

// check if value exist, if not, nothing is returned
function printNotNullPrice($value) {
	if ($value==0)
	    return "\n";
	elseif (!empty($value))
	    return " - $value AUD\n";
}


class OneStopPaymentsPlugin extends PaymethodPlugin {

	function getName() {
		return 'OneStopPayments';
	}

	function getDisplayName() {
		return Locale::translate('plugins.paymethod.onestoppayments.displayName');
	}

	function getDescription() {
		return Locale::translate('plugins.paymethod.onestoppayments.description');
	}   

	function register($category, $path) {
		if (parent::register($category, $path)) {			
			$this->addLocaleData();
			$this->import('OneStopPaymentsDAO');
			$OneStopPaymentsDAO =& new OneStopPaymentsDAO();
			DAORegistry::registerDAO('OneStopPaymentsDAO', $OneStopPaymentsDAO);
			return true;
		}
		return false;
	}

	function getSettingsFormFieldNames() {
		return array('onestoppaymentsurl','g_emailFrom','g_emailsubject','g_emailfooter','g_returnWords','g_Name','g_hashkey');
	}
	
	function isCurlInstalled() {
		return (function_exists('curl_init'));
	}

	function isConfigured() {
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return false;

		// Make sure CURL support is included.
		if (!$this->isCurlInstalled()) return false;

		// Make sure that all settings form fields have been filled in
		foreach ($this->getSettingsFormFieldNames() as $settingName) {
			$setting = $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), $settingName);
			if (empty($setting)) return false;
		}
		return true;
	}

	function displayPaymentSettingsForm(&$params, &$smarty) {
		$smarty->assign('isCurlInstalled', $this->isCurlInstalled());
		return parent::displayPaymentSettingsForm($params, $smarty);
	}

	function displayPaymentForm($queuedPaymentId, &$queuedPayment) {
		if (!$this->isConfigured()) return false;
		$schedConf =& Request::getSchedConf();
		$user =& Request::getUser();
		
		$params = array(
		    'TRAN-TYPE' => '810',
			'FNAME'  => ($user)?$user->getFirstName():'',
			'SNAME'  => ($user)?$user->getLastname():'',
			'Email'  => ($user)?$user->getEmail():'',
			'conference' => $schedConf->getConference()->getPath(),
			'schedConf' => $schedConf->getPath(),
			'UnitAmountIncTax'  => $queuedPayment->getAmount(),
			'OSCREGO'  => $queuedPaymentId,
			/*
			'g_Name'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_courseCode'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_startUrl'  => Request::url(null, null, 'payment', 'plugin', array($this->getName())),
			'g_returnURL'  => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'return')),
			'g_totalamount'  => $queuedPayment->getAmount(),
			'g_School'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_School'),
			'g_FirstName'  => ($user)?$user->getFirstName():'',
			'g_LastName'  => ($user)?$user->getLastname():'',
			'g_Category'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Category'),
			'g_CostCentre'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_CostCentre'),
			'g_emailFrom'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailFrom'),
			'g_emailbcc'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailFrom'),
			'g_emailsubject'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailsubject'),
			'g_emailfooter'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_emailfooter'),
			'g_returnWords'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_returnWords'),
			'g_PostURL'  => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'ipn')),
			'g_ref'  => $queuedPaymentId,
			'g_key'  => '',
			'g_itemdesc1'  => $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_Name'),
			'g_itemamount1'  => $queuedPayment->getAmount()
			*/
		);

		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign('params', $params);
		$templateMgr->assign('OneStopPaymentsUrl', $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'onestoppaymentsurl'));
		$templateMgr->display($this->getTemplatePath() . 'paymentForm.tpl');
	}

	/**
	 * Handle incoming requests/notifications
	 */
	function handle($args) {
		$templateMgr =& TemplateManager::getManager();
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return parent::handle($args);

		// Just in case we need to contact someone
		import('mail.MailTemplate');
		$contactName = $schedConf->getSetting('contactName');
		$contactEmail = $schedConf->getSetting('contactEmail');
		$mail = &new MailTemplate('ONESTOPPAYMENTS_INVESTIGATE');
		$mail->setFrom($contactEmail, $contactName);
		$mail->addRecipient($contactEmail, $contactName);

		$paymentStatus = Request::getUserVar('payment_status');
		switch (array_shift($args)) {
			case 'ipn':
				/* $debugmail = &new MailTemplate('ONESTOPPAYMENTS_INVESTIGATE');
				$debugmail->setFrom($contactEmail, $contactName);
				$debugmail->addRecipient('tepb@onestop.ac.uk', $contactName);
				$debugmail->assignParams(array(
					'schedConfName' => $schedConf->getFullTitle(),
					'postInfo' => print_r($_POST, true),
					'additionalInfo' => "Debug Email",
					'serverVars' => print_r($_SERVER, true)
				));
				$debugmail->send();
				*/

				$OneStopPaymentsDAO =& DAORegistry::getDAO('OneStopPaymentsDAO');
				$oscrego=Request::getUserVar('OSCREGO');
				if ($OneStopPaymentsDAO->transactionExists($oscrego)) {
					// A duplicate transaction was received; notify someone.
					$mail->assignParams(array(
						'schedConfName' => $schedConf->getFullTitle(),
						'postInfo' => print_r($_POST, true),
						'additionalInfo' => "Duplicate transaction ID: $oscrego",
						'serverVars' => print_r($_SERVER, true)
					));
					$mail->send();
					exit();
				} else {
					import('payment.ocs.OCSPaymentManager');
					$ocsPaymentManager =& OCSPaymentManager::getManager();
					$queuedPayment =& $ocsPaymentManager->getQueuedPayment($oscrego);
					if (!$queuedPayment) {
						// The queued payment entry is missing. Complain.
						$mail->assignParams(array(
							'schedConfName' => $schedConf->getFullTitle(),
							'postInfo' => print_r($_POST, true),
							'additionalInfo' => "Missing queued payment ID: $oscrego",
							'serverVars' => print_r($_SERVER, true)
						));
						$mail->send();
						exit();
					}

					//success or error?
					$respmessage = Request::getUserVar('RespMessage');
					$errmessage = Request::getUserVar('ErrorMessage');
					if (!strlen($respmessage) && !strlen($errmessage)) { //success url bug
						$successurlbug=1;
						$respmessage='Success';
					}
					
					if ($respmessage == 'Success') {

						$hash = Request::getUserVar('HASH');
						$receiptno = Request::getUserVar('ReceiptNo');
						$totamt = Request::getUserVar('TotAmt');
						$bankref = Request::getUserVar('BankRef');
						if ($successurlbug) {
							$bankref = Request::getUserVar('RespMessageBankRef');
						}
						$cur_conference = Request::getUserVar('conference');
						$cur_schedconf = Request::getUserVar('schedConf');
						
						//Hash control
						if (strlen($hash)) {
							$hashkey = $this->getSetting($schedConf->getConferenceId(), $schedConf->getSchedConfId(), 'g_hashkey');
							$tmp_querystr = substr($_SERVER['QUERY_STRING'],0,-38).$hashkey;
/*							$tmp_querystr = 'page=payment&op=plugin&path%5B%5D=OneStopPayments&path%5B%5D=ipn';
							$tmp_querystr .= '&RespMessage=Success';
							$tmp_querystr .= '&ReceiptNo='.$receiptno;
							$tmp_querystr .= '&TotAmt='.$totamt;
							$tmp_querystr .= '&conference='.$cur_conference;
							$tmp_querystr .= '&OSCREGO='.$oscrego;
							$tmp_querystr .= '&schedConf='.$cur_schedconf;
							$tmp_querystr .= '&BankRef='.$bankref;
							$tmp_querystr .= 'Key123';
*/							
							if ($hash == md5($tmp_querystr)) {
							} else {
								echo "Request is compromised.</br>";
								exit();
							}
						}

						$orig_payment_sum=$queuedPayment->getAmount();
						/*
						if ($orig_payment_sum==$totamt) {
						} else {
							echo "Paid sum and order sum mismatch. Please contact conference administrator for further investigation.";
							exit();
						}
						*/
						
						// New transaction succeeded. Record it.
						$OneStopPaymentsDAO->insertTransaction(
							$oscrego,
							$receiptno,
							$totamt,
							$bankref
						);

						//Get registration number
						$registration_id = $queuedPayment->assocId;
						
						// Fulfill the queued payment.
						if ($ocsPaymentManager->fulfillQueuedPayment($oscrego, $queuedPayment)) {
							// Send the registrant a notification that their payment was received
							$schedConfSettingsDao =& DAORegistry::getDAO('SchedConfSettingsDAO');
							$schedConfId = $schedConf->getId();

							$registrationName = $schedConfSettingsDao->getSetting($schedConfId, 'registrationName');
							$registrationEmail = $schedConfSettingsDao->getSetting($schedConfId, 'registrationEmail');
							$registrationPhone = $schedConfSettingsDao->getSetting($schedConfId, 'registrationPhone');
							$registrationFax = $schedConfSettingsDao->getSetting($schedConfId, 'registrationFax');
							$registrationMailingAddress = $schedConfSettingsDao->getSetting($schedConfId, 'registrationMailingAddress');
							$registrationContactSignature = $registrationName;

							if ($registrationMailingAddress != '') $registrationContactSignature .= "\n" . $registrationMailingAddress;
							if ($registrationPhone != '') $registrationContactSignature .= "\n" . Locale::Translate('user.phone') . ': ' . $registrationPhone;
							if ($registrationFax != '')	$registrationContactSignature .= "\n" . Locale::Translate('user.fax') . ': ' . $registrationFax;

							$registrationContactSignature .= "\n" . Locale::Translate('user.email') . ': ' . $registrationEmail;

							//Get order data
							$order_data = $OneStopPaymentsDAO->getOrderData($registration_id);

							$paramArray = array(
								'registrantName' => $schedConf->getSetting('contactName'),
								'schedConfName' => $schedConf->getFullTitle(),
								'registrationContactSignature' => $registrationContactSignature,
								'sumPaid' => $totamt,
								'orderDateRegistered' => $order_data[0]['date_registered'],
								'orderDatePaid' => $order_data[0]['date_paid'],
								'orderSpecialRequests' => $order_data[0]['special_requests'],
								'orderCurrencyCode' => $order_data[0]['currency_code_alpha'],
								'orderRegistrationType' => $order_data[0]['type_name'],
								'orderRegistrationPrice' => $order_data[0]['type_cost'],
								'orderOption1' => printNotEmptyOpt($order_data[0]['option_name']),
								'orderPrice1' => printNotNullPrice($order_data[0]['option_cost']),
								'orderOption2' => printNotEmptyOpt($order_data[1]['option_name']),
								'orderPrice2' => printNotNullPrice($order_data[1]['option_cost']),
								'orderOption3' => printNotEmptyOpt($order_data[2]['option_name']),
								'orderPrice3' => printNotNullPrice($order_data[2]['option_cost']),
								'orderOption4' => printNotEmptyOpt($order_data[3]['option_name']),
								'orderPrice4' => printNotNullPrice($order_data[3]['option_cost']),
								'orderOption5' => printNotEmptyOpt($order_data[4]['option_name']),
								'orderPrice5' => printNotNullPrice($order_data[4]['option_cost']),
								'orderOption6' => printNotEmptyOpt($order_data[5]['option_name']),
								'orderPrice6' => printNotNullPrice($order_data[5]['option_cost']),
								'orderOption7' => printNotEmptyOpt($order_data[6]['option_name']),
								'orderPrice7' => printNotNullPrice($order_data[6]['option_cost']),
								'orderOption8' => printNotEmptyOpt($order_data[7]['option_name']),
								'orderPrice8' => printNotNullPrice($order_data[7]['option_cost']),
								'orderOption9' => printNotEmptyOpt($order_data[8]['option_name']),
								'orderPrice9' => printNotNullPrice($order_data[8]['option_cost']),
								'orderOption10' => printNotEmptyOpt($order_data[9]['option_name']),
								'orderPrice10' => printNotNullPrice($order_data[9]['option_cost']),
								'orderOption11' => printNotEmptyOpt($order_data[10]['option_name']),
								'orderPrice11' => printNotNullPrice($order_data[10]['option_cost']),
								'orderOption12' => printNotEmptyOpt($order_data[11]['option_name']),
								'orderPrice12' => printNotNullPrice($order_data[11]['option_cost']),
								'orderOption13' => printNotEmptyOpt($order_data[12]['option_name']),
								'orderPrice13' => printNotNullPrice($order_data[12]['option_cost']),
								'orderOption14' => $order_data[13]['option_name'],
								'orderPrice14' => $order_data[13]['option_cost'],
								'orderOption15' => $order_data[14]['option_name'],
								'orderPrice15' => $order_data[14]['option_cost'],
								'optionsTotal' => $totamt - $order_data[0]['type_cost'],
							);

							$userDao =& DAORegistry::getDAO('UserDAO');
							$user = &$userDao->getUser($queuedPayment->getUserId());
							$useremail=$user->getEmail();

							$userFullName =$user->getFullName();

							import('mail.MailTemplate');
							$mail = new MailTemplate('MANUAL_PAYMENT_RECEIVED');
							$mail->setFrom($registrationEmail, $registrationName);
							$mail->assignParams($paramArray);
							$mail->addRecipient($useremail, $userFullName);
							$mail->send();
							
							header('Location: http://'.$_SERVER['SERVER_NAME'].'/ocs/index.php?conference='.$cur_conference.'&schedConf='.$cur_schedconf.'&page=payment&op=landing');
							exit();
						}
						// If we're still here, it means the payment couldn't be fulfilled.
						$mail->assignParams(array(
							'schedConfName' => $schedConf->getFullTitle(),
							'postInfo' => print_r($_POST, true),
							'additionalInfo' => "Queued payment ID $queuedPaymentId could not be fulfilled.",
							'serverVars' => print_r($_SERVER, true)
						));
						$mail->send();
					}
				} //end of success
				if (strlen($errmessage)) {
					echo "Payment unsuccessful. Error message: ".$errmessage;
				}
				exit();
			case 'return':
				Request::redirect(null, null, 'index');
				break;
		}
		parent::handle($args); // Don't know what to do with it
	}

	function getInstallSchemaFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'schema.xml');
	}

	function getInstallDataFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'data.xml');
	}
}

?>
