{**
 * paymentForm.tpl
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for submitting a OneStopPayments payment
 *
 *}
{assign var="pageTitle" value="plugins.paymethod.onestoppayments"}
{include file="common/header.tpl"}
 
<h3>{translate key="plugins.paymethod.onestoppayments.pay.bank"}</h3>
<p>{translate key="plugins.paymethod.onestoppayments.warning.bank.part1"} <a href="{translate key="plugins.paymethod.onestoppayments.warning.bank.link"}">{translate key="plugins.paymethod.onestoppayments.warning.bank.part2"}</a>{translate key="plugins.paymethod.onestoppayments.warning.bank.part3"}</p>
<h3>{translate key="plugins.paymethod.onestoppayments.pay.creditcard"}</h3>
<p>{translate key="plugins.paymethod.onestoppayments.warning.creditcard"}</p>

<form action="{$OneStopPaymentsUrl}" id="onestoppaymentsPaymentForm" name="onestoppaymentsPaymentForm" method="get" style="margin-bottom: 0px;">
	{include file="common/formErrors.tpl"}
	{if $params.amount}
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%">{translate key="plugins.paymethod.onestoppayments.purchase.amount"}</td>
			<td class="value" width="80%"><strong>{$params.amount|string_format:"%.2f"}{if $params.currency_code} ({$params.currency_code|escape}){/if}</strong></td>
		</tr>
	</table>
	{/if}
	{foreach from=$params key="name" item="value"}
		<input type="hidden" name="{$name|escape}" value="{$value|escape}" />
	{/foreach}

	<p><input type="submit" name="submitBtn" value="{translate key="common.continue"}" class="button defaultButton" /></p>
</form>

{include file="common/footer.tpl"}
