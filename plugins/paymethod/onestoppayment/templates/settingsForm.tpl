{**
 * settingsForm.tpl
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for OneStopPayments settings.
 *
 *}
	<tr>
		<td colspan="2"><h4>{translate key="plugins.paymethod.onestoppayments.settings"}</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="onestoppaymentsurl" required="true" key="plugins.paymethod.onestoppayments.settings.onestoppaymentsurl"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="onestoppaymentsurl" id="onestoppaymentsurl" size="50" value="{$onestoppaymentsurl|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.onestoppaymentsurl.description"}<br/>
			&nbsp;
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailFrom" required="true" key="plugins.paymethod.onestoppayments.settings.g_emailFrom"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailFrom" id="g_emailFrom" value="{$g_emailFrom|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_emailFrom.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailsubject" required="true" key="plugins.paymethod.onestoppayments.settings.g_emailsubject"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailsubject" id="g_emailsubject" value="{$g_emailsubject|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_emailsubject.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_emailfooter" required="true" key="plugins.paymethod.onestoppayments.settings.g_emailfooter"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_emailfooter" id="g_emailfooter" value="{$g_emailfooter|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_emailfooter.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_returnWords" required="true" key="plugins.paymethod.onestoppayments.settings.g_returnWords"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_returnWords" id="g_returnWords" value="{$g_returnWords|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_returnWords.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_Name" required="true" key="plugins.paymethod.onestoppayments.settings.g_Name"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_Name" id="g_Name" value="{$g_Name|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_Name.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="g_hashkey" required="true" key="plugins.paymethod.onestoppayments.settings.g_hashkey"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="g_hashkey" id="g_hashkey" value="{$g_hashkey|escape}" /><br/>
			{translate key="plugins.paymethod.onestoppayments.settings.g_hashkey.description"}
		</td>
	</tr>
	{if !$isCurlInstalled}
		<tr>
			<td colspan="2">
				<span class="instruct">{translate key="plugins.paymethod.onestoppayments.settings.curlNotInstalled"}</span>
			</td>
		</tr>
	{/if}
