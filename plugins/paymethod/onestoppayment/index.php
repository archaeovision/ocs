<?php

/**
 * @defgroup plugins_paymethod_onestoppayments
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for OneStopPayments plugin.
 *
 * @ingroup plugins_paymethod_sotonpayments
 */

require_once('OneStopPaymentsPlugin.inc.php');

return new OneStopPaymentsPlugin();

?> 
