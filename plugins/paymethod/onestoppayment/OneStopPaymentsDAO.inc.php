<?php

/**
 * @file OneStopPaymentsDAO.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class OneStopPaymentsDAO
 * @ingroup plugins_paymethod_onestoppayments
 * @see OneStopPaymentsPlugin
 *
 * @brief Class for OneStopPayments Logging DAO.
 * Operations for retrieving and modifying Transactions objects.
 */

import('db.DAO');

class OneStopPaymentsDAO extends DAO {

	/**
	 * Constructor.
	 */
	function OneStopPaymentsDAO() {
		parent::DAO();
	}

	/*
	 * Insert a payment into the payments table
	 */
	function insertTransaction($oscrego, $receiptno, $totamt, $bankref) {
		$ret = $this->update(
			'INSERT INTO onestoppayments_transactions (
				oscrego,
				receiptno,
				totamt,
				bankref)
				VALUES 
				(?, ?, ?, ?)'
			, 
			array(
				$oscrego,
				$receiptno,
				$totamt,
				$bankref
			) 
		);

		return true;
	}

	function transactionExists($oscrego) {
		$result =& $this->retrieve(
			'SELECT count(*) 
				FROM onestoppayments_transactions
				WHERE oscrego = ?', 
				array($oscrego)
		);

		$returner = false;
		if (isset($result->fields[0]) && $result->fields[0] >= 1) 
			$returner = true;

		$result->Close();
		return $returner;		
	}
	
	function getOrderData($registration_id) {
		$result =& $this->retrieve(
			"select R.date_registered, R.date_paid, R.special_requests, RT.currency_code_alpha, RT.cost as type_cost, RTS.setting_value as type_name, ROC.cost as option_cost, ROS.setting_value as option_name from registrations R 
				left outer join registration_types RT on RT.type_id=R.type_id
				left outer join registration_type_settings RTS on RTS.type_id=R.type_id and RTS.locale='en_US' and RTS.setting_name='name'
				left outer join registration_option_assoc ROA on ROA.registration_id=R.registration_id
				left outer join registration_option_costs ROC on ROC.option_id=ROA.option_id and ROC.type_id=R.type_id
				left outer join registration_option_settings ROS on ROS.option_id=ROA.option_id and ROS.locale='en_US' and ROS.setting_name='name'
			where R.registration_id = ?",
			array($registration_id)
		);

		$returner = array();
		if ($result->RecordCount() != 0) {
			while (!$result->EOF) {
				$returner[] =& $result->GetRowAssoc(false);
				$result->moveNext();
			}
			
		}

		$result->Close();
		unset($result);

		return $returner;
	}
	
}

?>
