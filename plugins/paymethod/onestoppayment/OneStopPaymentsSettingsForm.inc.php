<?php

/**
 * @file OneStopPaymentsSettingsForm.inc.php
 *
 * Copyright (c) 2010 Tim Boardman
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class OneStopPaymentsSettingsForm
 * @ingroup plugins_paymethod_onestoppayments
 * @see OneStopPaymentsPlugin
 *
 * @brief Form for conference managers to edit the OneStopPayments Settings
 * 
 */
 
import('form.Form');

class OneStopPaymentsSettingsForm extends Form {
	/** @var $schedConfId int */
	var $schedConfId;

	/** @var $plugin object */
	var $plugin;

	/** $var $errors string */
	var $errors;

	/**
	 * Constructor
	 * @param $schedConfId int
	 */
	function OneStopPaymentsSettingsForm(&$plugin, $conferenceId, $schedConfId) {
		parent::Form($plugin->getTemplatePath() . 'settingsForm.tpl');

		$this->addCheck(new FormValidatorPost($this));

		$this->conferenceId = $conferenceId;
		$this->schedConfId = $schedConfId;
		$this->plugin =& $plugin;

	}



	/**
	 * Initialize form data from current group group.
	 */
	function initData( ) {
		$schedConfId = $this->schedConfId;
		$conferenceId = $this->conferenceId;
		$plugin =& $this->plugin;

		$this->_data = array(
			'enabled' => $plugin->getSetting($conferenceId, $schedConfId, 'enabled'),
			'onestoppaymentsurl' => $plugin->getSetting($conferenceId, $schedConfId, 'onestoppaymentsurl'),
			'g_returnWords' => $plugin->getSetting($conferenceId, $schedConfId, 'g_returnWords'),
			'g_emailfooter' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailfooter'),
			'g_emailsubject' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailsubject'),
			'g_emailFrom' => $plugin->getSetting($conferenceId, $schedConfId, 'g_emailFrom'),
			'g_Name' => $plugin->getSetting($conferenceId, $schedConfId, 'g_Name'),
			'g_hashkey' => $plugin->getSetting($conferenceId, $schedConfId, 'g_hashkey'),
		);

	}

	/**
	 * Assign form data to user-submitted data.
	 */
	function readInputData() {
		$this->readUserVars(array('enabled',
								'onestoppaymentsurl', 
								'g_returnWords', 
								'g_emailfooter', 
								'g_emailsubject', 
								'g_emailFrom', 
								'g_Category', 
								'g_School', 
								'g_CostCentre',
								'g_Name',
								'g_hashkey'
								));
	}

	/**
	 * Save page - write to content file. 
	 */	 
	function save() {
		$plugin =& $this->plugin;
		$conferenceId = $this->conferenceId;
		$schedConfId = $this->schedConfId;
		$onestoppaymentsSettings = array();
		$plugin->updateSetting($conferenceId, $schedConfId, 'enabled', $this->getData('enabled'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'onestoppaymentsurl', $this->getData('onestoppaymentsurl'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_returnWords', $this->getData('g_returnWords'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailfooter', $this->getData('g_emailfooter'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailsubject', $this->getData('g_emailsubject'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_emailFrom', $this->getData('g_emailFrom'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_Name', $this->getData('g_Name'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'g_hashkey', $this->getData('g_hashkey'));
		
	}
}

?>
