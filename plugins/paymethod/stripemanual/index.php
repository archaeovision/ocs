<?php

/**
 * @defgroup plugins_paymethod_stripemanual
 */
 
/**
 * @file index.php
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Wrapper for Stripe plugin.
 *
 * @ingroup plugins_paymethod_stripemanual
 */

//$Id$

require_once('StripemanualPlugin.inc.php');

return new StripemanualPlugin();

?> 
