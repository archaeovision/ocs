<?php

/**
 * @file StripemanualSettingsForm.inc.php
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class StripemanualSettingsForm
 * @ingroup plugins_paymethod_stripemanual
 * @see StripemanualPlugin
 *
 * @brief Form for conference managers to edit the Stripemanual Settings
 * 
 */
 
//$Id$

import('form.Form');

class StripemanualSettingsForm extends Form {
	/** @var $schedConfId int */
	var $schedConfId;

	/** @var $plugin object */
	var $plugin;

	/** $var $errors string */
	var $errors;

	/**
	 * Constructor
	 * @param $schedConfId int
	 */
	function StripemanualSettingsForm(&$plugin, $conferenceId, $schedConfId) {
		parent::Form($plugin->getTemplatePath() . 'settingsForm.tpl');

		$this->addCheck(new FormValidatorPost($this));

		$this->conferenceId = $conferenceId;
		$this->schedConfId = $schedConfId;
		$this->plugin =& $plugin;

	}



	/**
	 * Initialize form data from current group group.
	 */
	function initData( ) {
		$schedConfId = $this->schedConfId;
		$conferenceId = $this->conferenceId;
		$plugin =& $this->plugin;

		/* FIXME: put these defaults somewhere else */
		/*
		$stripemanualSettings['enabled'] = true;
		$stripemanualSettings['stripemanualurl'] = "http://www.sandbox.stripe.com";
		$stripemanualSettings['selleraccount'] = "seller@ojs.org";
		;
		*/

		$this->_data = array(
			'enabled' => $plugin->getSetting($conferenceId, $schedConfId, 'enabled'),
			'stripemanualurl' => $plugin->getSetting($conferenceId, $schedConfId, 'stripemanualurl'),
			'secret_key' => $plugin->getSetting($conferenceId, $schedConfId, 'secret_key'),
			'public_key' => $plugin->getSetting($conferenceId, $schedConfId, 'public_key'),
			'reg_keyword' => $plugin->getSetting($conferenceId, $schedConfId, 'reg_keyword'),
			'discount_keyword' => $plugin->getSetting($conferenceId, $schedConfId, 'discount_keyword'),
			'mem_list_full' => $plugin->getSetting($conferenceId, $schedConfId, 'mem_list_full'),
			'mem_list_discounted' => $plugin->getSetting($conferenceId, $schedConfId, 'mem_list_discounted'),
			'skip_keywords' => $plugin->getSetting($conferenceId, $schedConfId, 'skip_keywords'),
		);

	}

	/**
	 * Assign form data to user-submitted data.
	 */
	function readInputData() {
		$this->readUserVars(array('enabled',
								'stripemanualurl', 
								'secret_key',
								'public_key',
								'reg_keyword',
								'discount_keyword',
								'skip_keywords',
								'mem_list_full',
								'mem_list_discounted'
								));
	}

	/**
	 * Save page - write to content file. 
	 */	 
	function save() {
		$plugin =& $this->plugin;
		$conferenceId = $this->conferenceId;
		$schedConfId = $this->schedConfId;

		$stripemanualSettings = array();
		$plugin->updateSetting($conferenceId, $schedConfId, 'enabled', $this->getData('enabled'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'stripemanualurl', $this->getData('stripemanualurl'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'secret_key',$this->getData('secret_key'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'public_key',$this->getData('public_key'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'reg_keyword',$this->getData('reg_keyword'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'discount_keyword',$this->getData('discount_keyword'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'skip_keywords',$this->getData('skip_keywords'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'mem_list_full',$this->getData('mem_list_full'));
		$plugin->updateSetting($conferenceId, $schedConfId, 'mem_list_discounted',$this->getData('mem_list_discounted'));
	}
}

?>
