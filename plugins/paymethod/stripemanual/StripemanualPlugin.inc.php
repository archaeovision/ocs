<?php

/**
 * @file StripemanualPlugin.inc.php
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class StripemanualPlugin
 * @ingroup plugins_paymethod_stripemanual
 * @see StripemanualDAO, StripemanualPaymentForm, StripemanualSettingsForm
 *
 * @brief Stripemanual plugin class
 *
 */

import('classes.plugins.PaymethodPlugin');
import('db.DAO');

require_once('lib/Stripe.php');

/*
function sm_debug($msg) {
//	if (($fh = fopen("/kunden/homepages/5/d386757553/htdocs/caaconference.org/ocs/debug.log", "a"))) {
	if (($fh = fopen("/home/vhosts/test.skinnymob.com/ocs/debug.log", "a"))) {
		fwrite($fh, '['.date('d.m.Y H:i:s').'] '.$msg."\n");
		fclose($fh);
	}
	return 1;
}
*/

class StripemanualPaymentsDAO extends DAO {

	/**
	 * Constructor.
	 */
	function StripemanualPaymentsDAO() {
		parent::DAO();
	}
	
	function getOrderData($registration_id) {
		$result =& $this->retrieve(
			"select R.date_registered, R.date_paid, R.special_requests, RT.currency_code_alpha, RT.cost as type_cost, RTS.setting_value as type_name, ROC.cost as option_cost, ROS.setting_value as option_name from registrations R 
				left outer join registration_types RT on RT.type_id=R.type_id
				left outer join registration_type_settings RTS on RTS.type_id=R.type_id and RTS.locale='en_US' and RTS.setting_name='name'
				left outer join registration_option_assoc ROA on ROA.registration_id=R.registration_id
				left outer join registration_option_costs ROC on ROC.option_id=ROA.option_id and ROC.type_id=R.type_id
				left outer join registration_option_settings ROS on ROS.option_id=ROA.option_id and ROS.locale='en_US' and ROS.setting_name='name'
			where R.registration_id = ?",
			array($registration_id)
		);

		$returner = array();
		if ($result->RecordCount() != 0) {
			while (!$result->EOF) {
				$returner[] =& $result->GetRowAssoc(false);
				$result->moveNext();
			}
			
		}

		$result->Close();
		unset($result);

		return $returner;
	}
	
}
	
class StripemanualPlugin extends PaymethodPlugin {

	function getName() {
		return 'Stripemanual';
	}

	function getDisplayName() {
		return __('plugins.paymethod.stripemanual.displayName');
	}

	function getDescription() {
		return __('plugins.paymethod.stripemanual.description');
	}

	function register($category, $path) {
		if (parent::register($category, $path)) {
			$this->addLocaleData();
			$StripemanualPaymentsDAO =& new StripemanualPaymentsDAO();
			DAORegistry::registerDAO('StripemanualPaymentsDAO', $StripemanualPaymentsDAO);
			return true;
		}
		return false;
	}

	function getSettingsFormFieldNames() {
		return array('stripemanualurl', 'secret_key', 'public_key', 'wp_site', 'reg_keyword', 'discount_keyword', 'mem_list_full', 'mem_list_discounted', 'skip_keywords', 'stripemanualInstructions');
	}

	function isCurlInstalled() {
		return (function_exists('curl_init'));
	}

	function isConfigured() {
		$schedConf =& Request::getSchedConf();
		if (!$schedConf) return false;

		// Make sure CURL support is included.
		if (!$this->isCurlInstalled()) return false;

		// Make sure that all settings form fields have been filled in
		foreach ($this->getSettingsFormFieldNames() as $settingName) {
			$setting = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), $settingName);
			if (empty($setting)) return false;
		}
		return true;
	}

	function displayPaymentSettingsForm(&$params, &$smarty) {
		$smarty->assign('isCurlInstalled', $this->isCurlInstalled());
		return parent::displayPaymentSettingsForm($params, $smarty);
	}

	function displayPaymentForm($queuedPaymentId, &$queuedPayment) {
		if (!$this->isConfigured()) return false;
		$schedConf =& Request::getSchedConf();
		$user =& Request::getUser();
		
		//Load settings
		$wp_site = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'wp_site');
		$reg_keyword = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'reg_keyword');
		$discount_keyword  = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'discount_keyword');
		$mem_list_full = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_full');
		$mem_list_discounted = $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_discounted');
		$skip_keywords = explode(",", $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'skip_keywords'));
		
		$fee_included = !(strstr($queuedPayment->getDescription(),$reg_keyword)===FALSE) || !(strstr($queuedPayment->getDescription(),$discount_keyword)===FALSE);
		$discounted_fee_included = !(strstr($queuedPayment->getDescription(),$discount_keyword)===FALSE);

		$memcheck_required = true;
		foreach ($skip_keywords as $skip_keyword) {
			if ($memcheck_required) {	//doesn't have match yet
				$memcheck_required = (strstr($queuedPayment->getDescription(),trim($skip_keyword))===FALSE);
			}
		}
		if ($memcheck_required) {
			// Check user in Wordpress
			$ch = curl_init();
			$mac=md5('check'.$user->getEmail().'P22m433');
			curl_setopt($ch, CURLOPT_URL, $wp_site.'/?upaction=check&email='.urlencode($user->getEmail()).'&list='.$mem_list_full.'&dlist='.$mem_list_discounted.'&mac='.$mac);
//			sm_debug($wp_site.'/?upaction=check&email='.urlencode($user->getEmail()).'&list='.$mem_list_full.'&dlist='.$mem_list_discounted.'&mac='.$mac);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 4);
			if (! $getresult = curl_exec ($ch)) {
				$message = curl_error($ch);
				$memcheckstatus = 0;
			} else {
				$memcheckstatus = 1;
			}
			//sm_debug('memcheckstatus:'.$memcheckstatus);
			//sm_debug('curl_error:'.$message);
			curl_close ($ch);
			if (strlen($getresult)>1000) {	//result is big, cant be correct
				$memcheckstatus = 0;
			}

			//Check does selected registration type includes membership fee

			if (!$fee_included) {
				//No membership fee included, check membership status
				if ($memcheckstatus) {
					switch ($getresult) {
						case 'not exists':
							//New user
						case 'membership not exists':
							//Existing user without membership
							$memcheckstatus = -1;	//No member, can't continue without membership fee
							break;
						default :
							//Validate expiration
							if (is_numeric($getresult)) {
								if ((int)$getresult>=(int)$schedConf->getSetting('endDate')) {
									$memcheckstatus = 1; //Membership OK
								} else {	
									$memcheckstatus = -4; //Membership expires before conference endh
								}
							} else {
								$memcheckstatus = -3; //Check error. Not valid date returned
							}
							
					}
				}
			} else {
				//Membership fee included, check does user exists
				if ($memcheckstatus) {
					switch ($getresult) {
						case 'not exists':
							//New user, let's create account
							$ch = curl_init();
							$mac=md5('register'.$user->getEmail().'P22m433');
							curl_setopt($ch, CURLOPT_URL, $wp_site.'/?upaction=register&email='.urlencode($user->getEmail()).'&firstname='.urlencode($user->getFirstName()).'&lastname='.urlencode($user->getLastName()).'&list='.($discounted_fee_included?$mem_list_discounted:$mem_list_full).'&mac='.$mac);
							//sm_debug($wp_site.'/?upaction=register&email='.urlencode($user->getEmail()).'&firstname='.urlencode($user->getFirstName()).'&lastname='.urlencode($user->getLastName()).'&list='.($discounted_fee_included?$mem_list_discounted:$mem_list_full).'&mac='.$mac);
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
							curl_setopt($ch, CURLOPT_TIMEOUT, 4);
							if (! $getresult = curl_exec ($ch)) {
								$message = curl_error($ch);
								$memchekstatus = 0;
							} else {
								if ($getresult=="user created successfully") {
									$memcheckstatus = 1;
								} else {
									$memchekstatus = -2; //New user creation failed
									$message = $getresult;
								}
							}
							//sm_debug('memcheckstatus:'.$memcheckstatus);
                        	//sm_debug('curl_error:'.$message);

							curl_close ($ch);
							break;
						case 'membership not exists':
							//Existing user without membership
							$memcheckstatus = -5;	//account exists, no membership 
							break;
					}
				}
			}

			if ($memcheckstatus<=0) {	//Errors, can't continue with payment.
				$templateMgr =& TemplateManager::getManager();
				switch ($memcheckstatus) {
					case 0: $templateMgr->assign('error0', 1); break;
					case -1: $templateMgr->assign('error1', 1); break;
					case -2: $templateMgr->assign('error2', 1); break;
					case -3: $templateMgr->assign('error3', 1); break;
					case -4: $templateMgr->assign('error4', 1); break;
					case -5: $templateMgr->assign('error5', 1); break;
				}
				$templateMgr->assign('errormessage', $message);
				$templateMgr->assign('confurl', $schedConf->getUrl());
			
				$templateMgr->display($this->getTemplatePath() . 'paymentErrorForm.tpl');
				return;
			}
		} //memcheck_required
		
		$cur_title = $schedConf->getLocalizedSetting('title');
		
		$params = array(
			'charset' => Config::getVar('i18n', 'client_charset'),
			'item_name' => $queuedPayment->getDescription(),
			'amount' => $queuedPayment->getAmount(),
			'currency_code' => $queuedPayment->getCurrencyCode(),
			'lc' => String::substr(AppLocale::getLocale(), 3),
			'custom' => $queuedPaymentId,
			'notify_url' => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'stripemanual')),
			'return' => $queuedPayment->getRequestUrl(),
			'cancel_return' => Request::url(null, null, 'payment', 'plugin', array($this->getName(), 'cancel')),
			'first_name' => ($user)?$user->getFirstName():'',
			'last_name' => ($user)?$user->getLastname():'',
			'confname' => $cur_title
		);
		$templateMgr =& TemplateManager::getManager();
		$templateMgr->assign('params', $params);
		$templateMgr->assign('stripemanualFormUrl', $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'stripemanualurl'));
		$templateMgr->assign('stripemanualPublicKey', $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'public_key'));
		$templateMgr->assign('stripemanualInstructions', $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'stripemanualInstructions'));
	
		$templateMgr->display($this->getTemplatePath() . 'paymentForm.tpl');
	}

	/**
	 * Handle incoming requests/notifications
	 */
	function handle($args) {
		$templateMgr =& TemplateManager::getManager();
		$schedConf =& Request::getSchedConf();
		$user =& Request::getUser();
		if (!$schedConf) return parent::handle($args);

		// Just in case we need to contact someone
/*		import('mail.MailTemplate');
		$contactName = $schedConf->getSetting('contactName');
		$contactEmail = $schedConf->getSetting('contactEmail');
		$mail = new MailTemplate('STRIPEMANUAL_INVESTIGATE_PAYMENT');
		$mail->setFrom($contactEmail, $contactName);
		$mail->addRecipient($contactEmail, $contactName);
*/

		$StripemanualPaymentsDAO =& DAORegistry::getDAO('StripemanualPaymentsDAO');


		switch (array_shift($args)) {
			case 'stripemanual': 	//Handling Stripe payment
				Stripe::setApiKey($this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'secret_key'));
				
				$queuedPaymentId = Request::getUserVar('custom');
				import('payment.ocs.OCSPaymentManager');
				$ocsPaymentManager =& OCSPaymentManager::getManager();
				// Verify the cost and user details as per Stripe spec.
				$queuedPayment =& $ocsPaymentManager->getQueuedPayment($queuedPaymentId);
				
				try {
					if (!isset($_POST['stripeToken'])) {
						throw new Exception("The Stripe Token was not generated correctly");
					}
					$payment = Stripe_Charge::create(array("amount" => $queuedPayment->getAmount()*100,
                                "currency" => $queuedPayment->getCurrencyCode(),
                                "card" => $_REQUEST['stripeToken'],
                                "receipt_email" =>  $_REQUEST['stripemanualEmail'],
								"description" => $queuedPayment->getDescription()." (".$_REQUEST['stripemanualEmail'].")"));
					if (!$payment->paid) {
						throw new Exception("Payment failed ".$payment->failure_message);
					}
					
					$response = "success";
					
					// Fulfill the queued payment.
					if ($ocsPaymentManager->fulfillQueuedPayment($queuedPaymentId, $queuedPayment)) {
						$response = "payment fulfilled";
						
						//Update account expiration
						$ch = curl_init();
						$mac=md5('extend'.$user->getEmail().date('Y',$schedConf->getSetting('endDate')).'P22m433');
						curl_setopt($ch, CURLOPT_URL, $this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'wp_site').'/?upaction=extend&year='.date('Y',$schedConf->getSetting('endDate')).'&email='.urlencode($user->getEmail()).'&list='.$this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_full').'&dlist='.$this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_discounted').'&mac='.$mac);
						//sm_debug($this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'wp_site').'/?upaction=extend&year='.date('Y',$schedConf->getSetting('endDate')).'&email='.urlencode($user->getEmail()).'&list='.$this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_full').'&dlist='.$this->getSetting($schedConf->getConferenceId(), $schedConf->getId(), 'mem_list_discounted').'&mac='.$mac);
						curl_setopt($ch, CURLOPT_HEADER, false);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER , true);
						curl_setopt($ch, CURLOPT_TIMEOUT, 4);
						if (! $getresult = curl_exec ($ch)) {
							$message = curl_error($ch);
							throw new Exception("Membership update failed.");
						};
						
			                        //sm_debug('curl_error:'.$message);


						// Send the registrant a notification that their payment was received
						$schedConfSettingsDao =& DAORegistry::getDAO('SchedConfSettingsDAO');
						
						$schedConfId = $schedConf->getId();
						$registrationName = $schedConfSettingsDao->getSetting($schedConfId, 'registrationName');
						$registrationEmail = $schedConfSettingsDao->getSetting($schedConfId, 'registrationEmail');
						$registrationPhone = $schedConfSettingsDao->getSetting($schedConfId, 'registrationPhone');
						$registrationFax = $schedConfSettingsDao->getSetting($schedConfId, 'registrationFax');
						$registrationMailingAddress = $schedConfSettingsDao->getSetting($schedConfId, 'registrationMailingAddress');
						$registrationContactSignature = $registrationName;

						if ($registrationMailingAddress != '') $registrationContactSignature .= "\n" . $registrationMailingAddress;
						if ($registrationPhone != '') $registrationContactSignature .= "\n" . Locale::Translate('user.phone') . ': ' . $registrationPhone;
						if ($registrationFax != '')	$registrationContactSignature .= "\n" . Locale::Translate('user.fax') . ': ' . $registrationFax;

						$registrationContactSignature .= "\n" . Locale::Translate('user.email') . ': ' . $registrationEmail;

						$userDao =& DAORegistry::getDAO('UserDAO');
						$user = &$userDao->getUser($queuedPayment->getUserId());
						$useremail=$user->getEmail();
						$userFullName =$user->getFullName();
						
						$orderData = $StripemanualPaymentsDAO->getOrderData($queuedPayment->assocId);
						$order = $orderData[0]['type_name']." - ".$orderData[0]['type_cost'].$orderData[0]['currency_code_alpha'];

						if (!empty($orderData[0]['special_requests'])) {
							$orderSpecialRequests = "SPECIAL REQUESTS\n".$orderData[0]['special_requests'];
						}
						
						foreach ($orderData as $item) {
							if (!empty($item['option_name'])) {
								$orderOptions .= $item['option_name']." - ".$item['option_cost'].$item['currency_code_alpha']."\n";
							}
						}
						
						if (!empty($orderData[0]['option_name'])) {
							$orderOptions = "OPTIONS\n".$orderOptions;
						}

						$paramArray = array(
							'registrantName' => $schedConf->getSetting('contactName'),
							'schedConfName' => $schedConf->getFullTitle(),
							'registrationContactSignature' => $registrationContactSignature ,
							'description' => $queuedPayment->getDescription(),
							'amount' => $queuedPayment->getAmount(),
							'currency' => $queuedPayment->getCurrencyCode(),
							'userFullName' => $userFullName,
							'orderOptions' => $orderOptions,
							'order' => $order,
							'orderSpecialRequests' => $orderSpecialRequests
							);

						import('mail.MailTemplate');
						
						$mail = new MailTemplate('MANUAL_PAYMENT_RECEIVED');
						$mail->setFrom($registrationEmail, $registrationName);
						$mail->assignParams($paramArray);
						$mail->addRecipient($useremail, $userFullName);
						$mail->send();
					}
				} catch (Exception $e) {
					$response = "error ".$e->getMessage();
				}
				
				if (!headers_sent()) {
					header('Location: '.$_REQUEST['return']);
					exit();
				}
				break;
			case 'notify':

				$queuedPaymentId = isset($args[0])?((int) $args[0]):0;
				import('payment.ocs.OCSPaymentManager');
				$ocsPaymentManager =& OCSPaymentManager::getManager();
				$queuedPayment =& $ocsPaymentManager->getQueuedPayment($queuedPaymentId);
				
				$userDao =& DAORegistry::getDAO('UserDAO');
				$user = &$userDao->getUser($queuedPayment->getUserId());
				$useremail=$user->getEmail();
				$userFullName =$user->getFullName();

				import('mail.MailTemplate');
				AppLocale::requireComponents(array(LOCALE_COMPONENT_APPLICATION_COMMON));
				$contactName = $schedConf->getSetting('registrationName');
				$contactEmail = $schedConf->getSetting('registrationEmail');
				$mail = new MailTemplate('MANUAL_PAYMENT_NOTIFICATION');
				$mail->setFrom($contactEmail, $contactName);
				$mail->addRecipient($useremail, $userFullName);
				$mail->addRecipient($contactEmail, $contactName);


				$mail->assignParams(array(
					'schedConfName' => $schedConf->getFullTitle(),
					'userFullName' => $user?$user->getFullName():('(' . __('common.none') . ')'),
					'userName' => $user?$user->getUsername():('(' . __('common.none') . ')'),
					'itemName' => $queuedPayment->getDescription(),
					'itemCost' => $queuedPayment->getAmount(),
					'itemCurrencyCode' => $queuedPayment->getCurrencyCode()
				));
				$mail->send();

				$templateMgr->assign(array(
					'currentUrl' => Request::url(null, null, null, 'payment', 'plugin', array('notify', $queuedPaymentId)),
					'pageTitle' => 'plugins.paymethod.stripemanual.paymentNotification',
					'message' => 'plugins.paymethod.stripemanual.notificationSent',
					'backLink' => Request::url(null, null, 'index'),
					'backLinkLabel' => 'common.continue'
				));
				$templateMgr->display('common/message.tpl');
				exit();
				break;
		}

		parent::handle($args); // Don't know what to do with it
	}

	function getInstallSchemaFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'schema.xml');
	}

	function getInstallEmailTemplatesFile() {
		return ($this->getPluginPath() . DIRECTORY_SEPARATOR . 'emailTemplates.xml');
	}

	function getInstallEmailTemplateDataFile() {
		return ($this->getPluginPath() . '/locale/{$installedLocale}/emailTemplates.xml');
	}
}

?>
