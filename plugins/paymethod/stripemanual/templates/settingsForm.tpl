{**
 * settingsForm.tpl
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for Stripemanual settings.
 *
 *}
	<tr>
		<td colspan="2"><h4>{translate key="plugins.paymethod.stripemanual.settings"}</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="stripemanualurl" required="true" key="plugins.paymethod.stripemanual.settings.stripemanualurl"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" name="stripemanualurl" id="stripemanualurl" size="50" value="{$stripemanualurl|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.stripemanualurl.description"}<br/>
			&nbsp;
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="secret_key" required="true" key="plugins.paymethod.stripemanual.settings.secret_key"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="secret_key" id="secret_key" value="{$secret_key|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.secret_key.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="public_key" required="true" key="plugins.paymethod.stripemanual.settings.public_key"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="public_key" id="public_key" value="{$public_key|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.public_key.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="wp_site" required="true" key="plugins.paymethod.stripemanual.settings.wp_site"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="wp_site" id="wp_site" value="{$wp_site|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.wp_site.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="reg_keyword" required="true" key="plugins.paymethod.stripemanual.settings.reg_keyword"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="reg_keyword" id="reg_keyword" value="{$reg_keyword|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.reg_keyword.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="discount_keyword" required="true" key="plugins.paymethod.stripemanual.settings.discount_keyword"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="discount_keyword" id="discount_keyword" value="{$discount_keyword|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.discount_keyword.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="skip_keywords" required="true" key="plugins.paymethod.stripemanual.settings.skip_keywords"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="32" name="skip_keywords" id="skip_keywords" value="{$skip_keywords|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.skip_keywords.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="mem_list_full" required="true" key="plugins.paymethod.stripemanual.settings.mem_list_full"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="8" name="mem_list_full" id="mem_list_full" value="{$mem_list_full|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.mem_list_full.description"}
		</td>
	</tr>
	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="mem_list_discounted" required="true" key="plugins.paymethod.stripemanual.settings.mem_list_discounted"}</td>
		<td class="value" width="80%">
			<input type="text" class="textField" size="8" name="mem_list_discounted" id="mem_list_discounted" value="{$mem_list_discounted|escape}" /><br/>
			{translate key="plugins.paymethod.stripemanual.settings.mem_list_discounted.description"}
		</td>
	</tr>
	{if !$isCurlInstalled}
		<tr>
			<td colspan="2">
				<span class="instruct">{translate key="plugins.paymethod.stripemanual.settings.curlNotInstalled"}</span>
			</td>
		</tr>
	{/if}

	<tr valign="top">
		<td class="label" width="20%">{fieldLabel name="stripemanualInstructions" required="true" key="plugins.paymethod.stripemanual.settings.instructions"}</td>
		<td class="value" width="80%">
			{translate key="plugins.paymethod.stripemanual.settings.stripemanualInstructions"}<br />
			<textarea name="stripemanualInstructions" id="stripemanualInstructions" rows="12" cols="60" class="textArea">{$stripemanualInstructions|escape}</textarea>
		</td>
	</tr>
