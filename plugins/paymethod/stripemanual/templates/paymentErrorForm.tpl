{**
 * paymentErrorForm.tpl
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for displaying Stripemanual payment errors
 *
 *}
{strip}
{assign var="pageTitle" value="plugins.paymethod.stripemanual"}
{include file="common/header.tpl"}
{/strip}

<table>
	<tr>
		<td>&nbsp;</td>
		<td>{$stripemanualDescription}</td>
	</tr>
</table>

{if $error0}
<p>{translate key="plugins.paymethod.stripemanual.error0"}</p>
{/if}
{if $error1}
<p>{translate key="plugins.paymethod.stripemanual.error1"}</p>
{/if}
{if $error2}
<p>{translate key="plugins.paymethod.stripemanual.error2"}</p>
{/if}
{if $error3}
<p>{translate key="plugins.paymethod.stripemanual.error3"}</p>
{/if}
{if $error4}
<p>{translate key="plugins.paymethod.stripemanual.error4"}</p>
{/if}
{if $error5}
<p>{translate key="plugins.paymethod.stripemanual.error5"}</p>
{/if}
{if $errormessage}
<p>{$errormessage}</p>
{/if}
<p>&raquo; <a href="{$confurl}&page=schedConf&op=registration">{translate key="plugins.paymethod.stripemanual.backtoreg"}</a></p>
{include file="common/footer.tpl"}
