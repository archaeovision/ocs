{**
 * paymentForm.tpl
 *
 * Copyright (c) 2006-2009 Gunther Eysenbach, Juan Pablo Alperin, MJ Suhonos
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Form for submitting a Stripemanual payment
 *
 *}
{strip}
{assign var="pageTitle" value="plugins.paymethod.stripemanual"}
{include file="common/header.tpl"}
{/strip}

{literal}
<script type="text/javascript">
  function selectmethod(obj) {
	if (obj.value=="stripe") {
		jQuery("#stripemanual_stripe").show();
		jQuery("#stripemanual_manual").hide();
	} else {
		jQuery("#stripemanual_stripe").hide();
		jQuery("#stripemanual_manual").show();
	}
  }
</script>
{/literal}

<table>
	<tr>
		<td>&nbsp;</td>
		<td>{$stripemanualDescription}</td>
	</tr>
</table>

	{if $params.amount}
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%">{translate key="plugins.paymethod.stripemanual.purchase.amount"}</td>
			<td class="value" width="80%"><strong>{$params.amount|string_format:"%.2f"}{if $params.currency_code} ({$params.currency_code|escape}){/if}</strong></td>			
		</tr>
	</table>
	{/if}
	{if $params.item_name}
	<table class="data" width="100%">
		<tr>
			<td class="label" width="20%">{translate key="plugins.paymethod.stripemanual.purchase.description"}</td>
			<td class="value" width="80%"><strong>{$params.item_name|escape}</strong></td>
		</tr>
	</table>
	{/if}


<h4>{translate key="plugins.paymethod.stripemanual.method"}</h4>

<p>{translate key="plugins.paymethod.stripemanual.warning"}</p>

<p><input type="radio" name="stripemanual_type" value="stripe" onclick="selectmethod(this);"> {translate key="plugins.paymethod.stripemanual.method.stripe"}</p>
<div id="stripemanual_stripe" style="display: none;">
<form action="{$params.notify_url}" id="stripemanualPaymentForm" name="stripemanualPaymentForm" method="post" style="margin-bottom: 0px;">
	{include file="common/formErrors.tpl"}
	{foreach from=$params key="name" item="value"}
		<input type="hidden" name="{$name|escape}" value="{$value|escape}" />
	{/foreach}

	<p>  <script src="{$stripemanualFormUrl}" class="stripe-button"
          data-key="{$stripemanualPublicKey}"
          data-amount="{$params.amount|escape}00" 
		  data-name="{$params.confname|escape}"
		  data-description="{$params.item_name|escape}"
		  data-currency="{$params.currency_code|escape}"></script>
	</p>
</form>
</div>

<p><input type="radio" name="stripemanual_type" value="manual" onclick="selectmethod(this);"> {translate key="plugins.paymethod.stripemanual.method.manual"}</p>
<div id="stripemanual_manual" style="display: none;">
<input type="hidden" name="custom" value="{$params.custom|escape}">
<table class="data" width="100%">
	<tr>
		<td class="label" width="20%">{translate key="plugins.paymethod.stripemanual.purchase.title"}</td>
		<td class="value" width="80%"><strong>{$params.confname|escape}</strong></td>
	</tr>
	{if $params.amount}
		<tr>
			<td class="label" width="20%">{translate key="plugins.paymethod.stripemanual.purchase.fee"}</td>
			<td class="value" width="80%"><strong>{$params.amount|string_format:"%.2f"}{if $params.currency_code} ({$params.currency_code|escape}){/if}</strong></td>
		</tr>
	{/if}
	{if $itemDescription}
	<tr>
		<td colspan="2">{$params.item_name|escape|nl2br}</td>
	</tr>
	{/if}
</table>
<p>{$stripemanualInstructions|nl2br}</p>

<p><a href="{url page="payment" op="plugin" path="Stripemanual"|to_array:"notify":$params.custom|escape}" class="action">{translate key="plugins.paymethod.stripemanual.sendNotificationOfPayment"}</a>
</div>
{include file="common/footer.tpl"}
